package com.mst.automation.ica.regressiontestcases;

import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;

public class WorkersCompensationNonClaimantType extends BaseTest{
	@Test (groups = "Regression Suite")
	
	
	public void nonclaimantrequest() throws Exception {
	
	String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
	String className = this.getClass().getSimpleName();
	String tcName = "TCICA_044";

	
	reporter = new ReportGenerator(getbrowser(), className);
	homePage = loginPage.signup();
	workerClaimPage = homePage.requestworker();
	workerClaimPage.fillingNonClaimant(methodName, tcName, reporter);
}


}
