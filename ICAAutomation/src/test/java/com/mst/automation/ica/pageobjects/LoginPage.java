package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.pageobjects.HomePage;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 10/26/2017 
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 10/26/2017 
 * Description: For Login page to enter the credentials
 *
 */
	
public class LoginPage extends DriverClass {

	 @FindBy(how = How.ID, using = "username")
	 public WebElement txtbxUserName;
	 	 
	 @FindBy(how = How.CSS, using = "input[placeholder='Email']")
	 public WebElement communityUserName;

	 @FindBy(how = How.ID, using = "password")
	 public WebElement txtbxPassword;
	 
	 @FindBy(how = How.CSS, using = "input[placeholder='Password']")
	 public WebElement communityPassword;

	 @FindBy(how = How.ID, using = "Login")
	 public WebElement lnkLogin;
	 
	 @FindBy(how = How.XPATH, using = "//span[contains(.,'Log in')]")
	 public WebElement communityLogin;
	 
	 public By element() {
			By ul=By.cssSelector("input[placeholder='Email']");
			return ul;
		}
	 
	 public LoginPage(WebDriver driver){
		 super(driver);
		 PageFactory.initElements(driver, this);
	 }
	 
	 public void setUserName(String username){
		 txtbxUserName.clear();
		 txtbxUserName.sendKeys(username);
	 }
	 
	 public void setCommunityUserName(String username){
		 SeleniumUtils.presenceOfElement(driver, element());
		 communityUserName.clear();
		 communityUserName.sendKeys(username);
	 }
	 
	 public void setPassword(String password){
		 txtbxPassword.clear();
		 txtbxPassword.sendKeys(password);
	 }
	 
	 public void setCommunityPassword(String password){
		 communityPassword.clear();
		 communityPassword.sendKeys(password);
	 }
	 
	 public void clickLogin(){
		 lnkLogin.click();
	 }
	 
	 public void clickCommunityLogin(){
		 communityLogin.click();
	 }
	 
	 /*To enter the credentials in the Login page*/
	 public HomePage login(String username, String password, ReportGenerator generator){
		 
		 setUserName(username);
		 generator.childReport("Username entered");
		 setPassword(password);
		 generator.childReport("Password entered");
		 clickLogin();
		 generator.childReport("Login clicked");
		 return new HomePage(driver);
	 }
	 public HomePage loginCommunity(String username, String password, ReportGenerator generator){
		 
		 setCommunityUserName(username);
		 generator.childReport("Username entered");
		 setCommunityPassword(password);
		 generator.childReport("Password entered");
		 clickCommunityLogin();
		 generator.childReport("Login clicked");
		 return new HomePage(driver);
	 }
	 
	 public HomePage signup(){
		 return new HomePage(driver);
	 }
	 public Form407Page formUrl(){
		 return new Form407Page(driver);
	 }
	 public Form446Page formUrl446(){
		 return new Form446Page(driver);
	 }
	 public Form101Page formUrl101(){
		 		 return new Form101Page(driver);
	 }
	
	 public Form102Page formUrl102() {
		 return new Form102Page(driver);
	 }
	 
	 public Form120Page formUrl120() {
		 return new Form120Page(driver);
	 }
	 
	 public Form121Page formUrl121() {
		 return new Form121Page(driver);
	 }
	 
	 public Form122Page formUrl122() {
		 return new Form122Page(driver);
	 } 
	 
	 public Form528Page formUrl528() {
		 return new Form528Page(driver);
	 }
	 
	 public Form529Page formUrl529() {
		 return new Form529Page(driver);
	 } 
	

}
