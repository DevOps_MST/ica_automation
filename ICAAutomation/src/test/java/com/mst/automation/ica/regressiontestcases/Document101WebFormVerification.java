package com.mst.automation.ica.regressiontestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class Document101WebFormVerification extends BaseTest{
@Test (groups = "Form Regression Suite")
@Parameters({ "userType", "UrlQA"})
	
	public void form101Document(String userType, String UrlQA) throws Exception {

		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_035";
		
		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");
		
		String Url2 = TestUtils.getStringFromPropertyFile(UrlQA);
		
		reporter = new ReportGenerator(getbrowser(), className);
		page101 = loginPage.formUrl101();
		
		page101.fillingForm101(methodName, tcName, reporter);
		SeleniumUtils.switchToNewTab(driver, Url2);
		homePage = loginPage.login(user, pwd, reporter);
		docuselect = homePage.documentObjClick(reporter);
		verify101 = docuselect.docu101Verify(methodName, tcName, reporter);
		verify101.verify101Document(methodName, tcName, reporter);

	}

}
