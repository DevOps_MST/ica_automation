package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

public class WorkersClaimPage extends DriverClass{
	
	@FindBy(xpath = "//h2[text()=\"Request for a Worker's Compensation Claim\"]")
	public WebElement pageVerify;
	
	public By element() {
		By open = By.xpath("//h2[text()=\"Request for a Worker's Compensation Claim\"]");
		return open;
	}
	
	@FindBy(xpath = "//label/span[text()=\"Claim number\"]/following::input[1]")
	public WebElement claimnumberEnter;
	
	@FindBy(xpath = "//form/div/div[1]/div[2]/span")
	public WebElement tickVerify;
	
	public By green() {
		By ver = By.xpath("//form/div/div[1]/div[2]/span");
		return ver;
	}
	
	@FindBy(css = "select[name=\"Controlling picklist\"]")
	public WebElement picklist;
	
	@FindBy(xpath = "//select/option[text()=\"Claimant\"]")
	public WebElement claimantClick;
	
	@FindBy(xpath = "//select/option[text()=\"Employer\"]")
	public WebElement employerClick;
	
	@FindBy(xpath = "//label/span[text()='First Name']/following::input[1]")
	public WebElement firstName;
	
	public By name() {
		By first = By.xpath("//label/span[text()='First Name']/following::input[1]");
		return first;
	}
	
	@FindBy(xpath = "//label/span[text()='Party type']/following::input[14]")
	public WebElement empfirstName;
	
	public By empname() {
		By emp = By.xpath("//label/span[text()='Party type']/following::input[14]");
		return emp;
	}
	
	
	@FindBy(xpath = "//label/span[text()='Last Name']/following::input[1]")
	public WebElement lastName;
	
	@FindBy(xpath = "//label/span[text()='Party type']/following::input[15]")
	public WebElement emplastName;
	
	@FindBy(xpath = "//label/span[text()='Date Of Birth']/following::input[1]")
	public WebElement dob;
	
	@FindBy(xpath = "//label/span[text()='Last 4 Digits Of SSN']/following::input[1]")
	public WebElement ssn;
	
	@FindBy(xpath = "//label/span[text()='Phone Number']/following::input[1]")
	public WebElement phone;
	
	@FindBy(xpath = "//label/span[text()='Party type']/following::input[16]")
	public WebElement empphone;
	
	@FindBy(xpath = "//label/span[text()='Fax Number']/following::input[1]")
	public WebElement fax;
	
	@FindBy(xpath = "//label/span[text()='Party type']/following::input[17]")
	public WebElement empfax;
		
	@FindBy(xpath = "//label/span[text()='HomeAddress']/following::input[1]")
	public WebElement address;
	
	@FindBy(xpath = "//label/span[text()='BusinessAddress']/following::input[1]")
	public WebElement businessAddress;
	
	@FindBy(xpath = "//label/span[text()='City']/following::input[1]")
	public WebElement city;
	
	@FindBy(xpath = "//label/span[text()='Party type']/following::input[19]")
	public WebElement empcity;
		
	@FindBy(xpath = "//label/span[text()='State']/following::input[1]")
	public WebElement state;
	
	@FindBy(xpath = "//label/span[text()='Party type']/following::input[20]")
	public WebElement empstate;
	
	
	@FindBy(xpath = "//label/span[text()='ZipCode']/following::input[1]")
	public WebElement zipcode;
	
	
	@FindBy(xpath = "//label/span[text()='Party type']/following::input[21]")
	public WebElement empzipcode;
	
	
	@FindBy(xpath = "//label/span[text()='Email']/following::input[1]")
	public WebElement email;
	
	@FindBy(xpath = "//label/span[text()='Party type']/following::input[22]")
	public WebElement empemail;
	
	
	@FindBy(xpath = "//label/span[text()='Confirm Email']/following::input[1]")
	public WebElement confirmEmail;
	
	@FindBy(xpath = "//label/span[text()='Party type']/following::input[23]")
	public WebElement empconfirmEmail;
	
	
	@FindBy(xpath = "//label/span[text()='Employer']/following::input[1]")
	public WebElement employer;
	
	@FindBy(xpath = "//label/span[text()='Organization']/following::input[1]")
	public WebElement organization;
	
	@FindBy(xpath = "//form/button[text()='Submit']")
	public WebElement submitClick;
	
	@FindBy(xpath = "//label/span[text()='Party type']/following::button[2]")
	public WebElement empsubmitClick;
	
	
	public By uploadVerify() {
		By uv = By.xpath("//div[2]/div[text()='Claims File Upload']");
		return uv;
	}
	
	@FindBy(xpath = "//div[2]/div[text()='Claims File Upload']")
	public WebElement fileuploadverify;	
	
	@FindBy(xpath = "//label/span[text()='Upload Files']")
	public WebElement clickUpload;	
	
	@FindBy(xpath = "//button[text()='Upload']")
	public WebElement afterClick;
	
	@FindBy(xpath = "//button[text()='Skip']")
	public WebElement skipClick;
	
	public By verifyMessage() {
		By vm = By.xpath("//div[2]/div[text()='Claims File Upload']");
		return vm;
	}
	
	@FindBy(xpath = "//div/fieldset/span")
	public WebElement verifyThanks;
	
	
	public WorkersClaimPage(WebDriver driver) {
		super(driver);
	}
	
	public void fillingClaimant(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(pageVerify, driver);
		generator.childReport("Page Verified");
		
		claimnumberEnter.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claim number"));
		generator.childReport("Claim Number Entered");
		
		/*SeleniumUtils.presenceOfElement(driver, green());
		SeleniumUtils.highLightElement(tickVerify, driver);*/
		picklist.click();
		claimantClick.click();
		generator.childReport("Claimant Clicked");
		
		SeleniumUtils.presenceOfElement(driver, name());
		SeleniumUtils.highLightElement(firstName, driver);
		firstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "First name"));
		generator.childReport("First name Entered");
		
		SeleniumUtils.highLightElement(lastName, driver);
		lastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Last name"));
		generator.childReport("Last name Entered");
		
		SeleniumUtils.highLightElement(dob, driver);
		dob.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Date Of Birth"));
		generator.childReport("Date of Birth Entered");
		
		SeleniumUtils.highLightElement(ssn, driver);
		ssn.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "ssn"));
		generator.childReport("SSN Entered");
		
		SeleniumUtils.highLightElement(phone, driver);
		phone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "phone"));
		generator.childReport("Phone Entered");
		
		SeleniumUtils.highLightElement(fax, driver);
		fax.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "fax"));
		generator.childReport("Fax Entered");
		
		SeleniumUtils.highLightElement(address, driver);
		address.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "address"));
		generator.childReport("Address Entered");
		
		SeleniumUtils.highLightElement(city, driver);
		city.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "city"));
		generator.childReport("City Entered");
		
		SeleniumUtils.highLightElement(state, driver);
		state.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "state"));
		generator.childReport("State Entered");
		
		SeleniumUtils.highLightElement(zipcode, driver);
		zipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "zipcode"));
		generator.childReport("ZipCode Entered");
		
		SeleniumUtils.highLightElement(email, driver);
		
		String var = SeleniumUtils.randomEmail();
		email.sendKeys(var);
		//email.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "email"));
		generator.childReport("Email Entered");
		
		SeleniumUtils.highLightElement(confirmEmail, driver);
		confirmEmail.sendKeys(var);
		generator.childReport("ConfirmEmail Entered");
		
		SeleniumUtils.highLightElement(employer, driver);
		employer.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employer"));
		generator.childReport("Employer Entered");
		
		SeleniumUtils.highLightElement(submitClick, driver);
		submitClick.click();
		generator.childReport("Submit button Clicked");
		
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, uploadVerify());
		SeleniumUtils.highLightElement(fileuploadverify, driver);
		generator.childReport("Upload Verified");
		
		SeleniumUtils.highLightElement(clickUpload, driver);
		clickUpload.click();
		
		SeleniumUtils.upload();
		generator.childReport("Upload Successfully");
		
		SeleniumUtils.highLightElement(afterClick, driver);
		afterClick.click();
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, verifyMessage());
		SeleniumUtils.highLightElement(verifyThanks, driver);
		generator.childReport("Successful Message Verified");
		
		
	}
public void fillingNonClaimant(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(pageVerify, driver);
		generator.childReport("Page Verified");
		
		claimnumberEnter.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claim number"));
		generator.childReport("Claim Number Entered");
		
		/*SeleniumUtils.presenceOfElement(driver, green());
		SeleniumUtils.highLightElement(tickVerify, driver);*/
		picklist.click();
		employerClick.click();
		generator.childReport("Employer Clicked");
		
		SeleniumUtils.presenceOfElement(driver, empname());
		SeleniumUtils.highLightElement(empfirstName, driver);
		empfirstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "First name"));
		generator.childReport("First name Entered");
		
		SeleniumUtils.highLightElement(emplastName, driver);
		emplastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Last name"));
		generator.childReport("Last name Entered");
		
		SeleniumUtils.highLightElement(empphone, driver);
		empphone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "phone"));
		generator.childReport("Phone Entered");
		
		SeleniumUtils.highLightElement(empfax, driver);
		empfax.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "fax"));
		generator.childReport("Fax Entered");
		
		SeleniumUtils.highLightElement(businessAddress, driver);
		businessAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "address"));
		generator.childReport("Business Address Entered");
		
		SeleniumUtils.highLightElement(empcity, driver);
		empcity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "city"));
		generator.childReport("City Entered");
		
		SeleniumUtils.highLightElement(empstate, driver);
		empstate.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "state"));
		generator.childReport("State Entered");
		
		SeleniumUtils.highLightElement(empzipcode, driver);
		empzipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "zipcode"));
		generator.childReport("ZipCode Entered");
		
		SeleniumUtils.highLightElement(empemail, driver);
		
		String var = SeleniumUtils.randomEmail();
		empemail.sendKeys(var);
		//email.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "email"));
		generator.childReport("Email Entered");
		
		SeleniumUtils.highLightElement(empconfirmEmail, driver);
		empconfirmEmail.sendKeys(var);
		generator.childReport("ConfirmEmail Entered");
		
		SeleniumUtils.highLightElement(organization, driver);
		organization.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "employer"));
		generator.childReport("Organization Entered");
		
		SeleniumUtils.highLightElement(empsubmitClick, driver);
		empsubmitClick.click();
		generator.childReport("Submit button Clicked");
		
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, uploadVerify());
		SeleniumUtils.highLightElement(fileuploadverify, driver);
		generator.childReport("Upload Verified");
		Thread.sleep(5000);
		skipClick.click();
		SeleniumUtils.presenceOfElement(driver, verifyMessage());
		SeleniumUtils.highLightElement(verifyThanks, driver);
		generator.childReport("Successful Message Verified");
		
		
	}
}
