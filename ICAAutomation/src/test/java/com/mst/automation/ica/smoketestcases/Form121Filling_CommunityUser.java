/**
 * 
 */
package com.mst.automation.ica.smoketestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.TestUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 22-Jan-2018
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form121Filling_CommunityUser extends BaseTest{
	
	@Test (groups = "Smoke Suite")
	@Parameters({ "userType" })
		
		public void formfilling121(String userType) throws Exception {
		
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_121";
		
		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");
		
		reporter = new ReportGenerator(getbrowser(), className);
		homePage = loginPage.loginCommunity(user, pwd, reporter);
		page121 = homePage.formUrl121cu();
		page121.fillingForm121Community(methodName, tcName, reporter);

	}


}
