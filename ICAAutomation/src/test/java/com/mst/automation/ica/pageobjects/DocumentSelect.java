package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class DocumentSelect extends DriverClass{
	
	@FindBy(xpath = "//li/span[text()='Documents']/following::span[2]")
	public WebElement docuList;
	
	public By listView() {
		By list = By.cssSelector("input[data-aura-class=\"uiInput uiInputTextForAutocomplete uiInput--default uiInput--input\"]");
		return list;
	}
	
	@FindBy(css = "input[data-aura-class=\"uiInput uiInputTextForAutocomplete uiInput--default uiInput--input\"]")
	public WebElement listBox;
	
	public By clickFrom() {
		By form = By.cssSelector("a[tabindex=\"-1\"]");
		return form;
	}
	
	@FindBy(css = "a[tabindex=\"-1\"]")
	public WebElement clickNumber;
	
	public By recordClick() {
		By rc = By.xpath("//table/tbody/tr[1]/th/span/a");
		return rc;
	}
	
	@FindBy(xpath = "//table/tbody/tr[1]/th/span/a")
	public WebElement firstRecord;
	
	public By DocuVerify() {
		By doc = By.xpath("//p[text()=\"Document\"]/following::h1/span");
		return doc;
	}
	
	@FindBy(xpath = "//p[text()=\"Document\"]/following::h1/span")
	public WebElement recordName;
	
	@FindBy(xpath = "//div/div[1]/span[text()=\"101\"]/following::a[1]")
	public WebElement record101Verify;
	
	@FindBy(xpath = "//div/div[1]/span[text()=\"407\"]/following::a[1]")
	public WebElement record407Verify;
	
	@FindBy(xpath = "//div/div[1]/span[text()=\"102\"]/following::a[1]")
	public WebElement record102Verify;
	
	@FindBy(xpath = "//div/div[1]/span[text()=\"446\"]/following::a[1]")
	public WebElement record446Verify;
	
	@FindBy(xpath = "//div/div[1]/span[text()=\"529\"]/following::a[1]")
	public WebElement record529Verify;
	
	@FindBy(xpath = "//div/div[1]/span[text()=\"121\"]/following::a[1]")
	public WebElement record121Verify;
	
	@FindBy(xpath = "//div/div[1]/span[text()=\"122\"]/following::a[1]")
	public WebElement record122Verify;
	
	@FindBy(xpath = "//div/div[1]/span[text()=\"104\"]/following::a[1]")
	public WebElement record104Verify;
	
	@FindBy(xpath = "//div/div[1]/span[text()=\"106\"]/following::a[1]")
	public WebElement record106Verify;
	
	@FindBy(xpath = "//div/div[1]/span[text()=\"107\"]/following::a[1]")
	public WebElement record107Verify;
	
	@FindBy(xpath = "//div/div[1]/span[text()=\"108\"]/following::a[1]")
	public WebElement record108Verify;
	
	@FindBy(xpath = "//div/div[1]/span[text()=\"528\"]/following::a[1]")
	public WebElement record528Verify;
	
	@FindBy(xpath = "//div/div[1]/span[text()=\"Owner\"]/following::span[1]")
	public WebElement ownerName;
	
	
	public By DocuCheck101() {
		By check = By.xpath("//p[text()=\"101\"]/following::span[1]");
		return check;
	}
	
	@FindBy(xpath = "//p[text()=\"101\"]/following::span[1]")
	public WebElement checkDocu101;
	
	
	public By DocuCheck407() {
		By check = By.xpath("//p[text()=\"407\"]/following::span[1]");
		return check;
	}
	
	@FindBy(xpath = "//p[text()=\"407\"]/following::span[1]")
	public WebElement checkDocu407;
	
	public By DocuCheck102() {
		By check = By.xpath("//p[text()=\"102\"]/following::span[1]");
		return check;
	}
	
	@FindBy(xpath = "//p[text()=\"102\"]/following::span[1]")
	public WebElement checkDocu102;
	
	public By DocuCheck446() {
		By check = By.xpath("//p[text()=\"446\"]/following::span[1]");
		return check;
	}
	
	@FindBy(xpath = "//p[text()=\"446\"]/following::span[1]")
	public WebElement checkDocu446;
	
	public By DocuCheck529() {
		By check = By.xpath("//p[text()=\"529\"]/following::span[1]");
		return check;
	}
	
	@FindBy(xpath = "//p[text()=\"529\"]/following::span[1]")
	public WebElement checkDocu529;
	
	
	public By DocuCheck121() {
		By check = By.xpath("//p[text()=\"121\"]/following::span[1]");
		return check;
	}
	
	@FindBy(xpath = "//p[text()=\"121\"]/following::span[1]")
	public WebElement checkDocu121;
	
	public By DocuCheck122() {
		By check = By.xpath("//p[text()=\"122\"]/following::span[1]");
		return check;
	}
	
	@FindBy(xpath = "//p[text()=\"122\"]/following::span[1]")
	public WebElement checkDocu122;
	
	public By DocuCheck104() {
		By check = By.xpath("//p[text()=\"104\"]/following::span[1]");
		return check;
	}
	
	@FindBy(xpath = "//p[text()=\"104\"]/following::span[1]")
	public WebElement checkDocu104;
	
	public By DocuCheck106() {
		By check = By.xpath("//p[text()=\"106\"]/following::span[1]");
		return check;
	}
	
	@FindBy(xpath = "//p[text()=\"106\"]/following::span[1]")
	public WebElement checkDocu106;
	
	public By DocuCheck107() {
		By check = By.xpath("//p[text()=\"107\"]/following::span[1]");
		return check;
	}
	
	@FindBy(xpath = "//p[text()=\"107\"]/following::span[1]")
	public WebElement checkDocu107;
	
	public By DocuCheck108() {
		By check = By.xpath("//p[text()=\"108\"]/following::span[1]");
		return check;
	}
	
	@FindBy(xpath = "//p[text()=\"108\"]/following::span[1]")
	public WebElement checkDocu108;
	
	public By DocuCheck528() {
		By check = By.xpath("//p[text()=\"528\"]/following::span[1]");
		return check;
	}
	
	@FindBy(xpath = "//p[text()=\"528\"]/following::span[1]")
	public WebElement checkDocu528;
	

	public DocumentSelect(WebDriver driver) {
		super(driver);
	}
	
	public Document101Page docu101Verify(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		docuList.click();
		generator.childReport("List View Clicked");
		
		SeleniumUtils.presenceOfElement(driver, listView());
		SeleniumUtils.highLightElement(listBox, driver);
		listBox.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Form no"));
		
		SeleniumUtils.presenceOfElement(driver, clickFrom());
		SeleniumUtils.highLightElement(clickNumber, driver);
		clickNumber.click();
		
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, recordClick());
		TestUtils.descendingOrder(driver,generator);
		
		SeleniumUtils.presenceOfElement(driver, recordClick());
		SeleniumUtils.highLightElement(firstRecord, driver);
		firstRecord.click();
		
		SeleniumUtils.presenceOfElement(driver, DocuVerify());
		SeleniumUtils.highLightElement(recordName, driver);
		
		SeleniumUtils.highLightElement(ownerName, driver);
		
		SeleniumUtils.highLightElement(record101Verify, driver);
		record101Verify.click();
		
		SeleniumUtils.presenceOfElement(driver, DocuCheck101());
		SeleniumUtils.highLightElement(checkDocu101, driver);
		return new Document101Page(driver);
	}
	
	
public Document407Page docu407Verify(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		docuList.click();
		generator.childReport("List View Clicked");
		
		SeleniumUtils.presenceOfElement(driver, listView());
		SeleniumUtils.highLightElement(listBox, driver);
		listBox.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Form no"));
		
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, clickFrom());
		SeleniumUtils.highLightElement(clickNumber, driver);
		clickNumber.click();
		
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, recordClick());
		TestUtils.descendingOrder(driver,generator);
		
		SeleniumUtils.presenceOfElement(driver, recordClick());
		SeleniumUtils.highLightElement(firstRecord, driver);
		firstRecord.click();
		
		SeleniumUtils.presenceOfElement(driver, DocuVerify());
		SeleniumUtils.highLightElement(recordName, driver);
		
		SeleniumUtils.highLightElement(ownerName, driver);
		
		SeleniumUtils.highLightElement(record407Verify, driver);
		record407Verify.click();
		
		SeleniumUtils.presenceOfElement(driver, DocuCheck407());
		SeleniumUtils.highLightElement(checkDocu407, driver);
		return new Document407Page(driver);
	}


public Document102Page docu102Verify(String methodName, String tcName, ReportGenerator generator) throws Exception {
	
	docuList.click();
	generator.childReport("List View Clicked");
	
	SeleniumUtils.presenceOfElement(driver, listView());
	SeleniumUtils.highLightElement(listBox, driver);
	listBox.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Form no"));
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, clickFrom());
	SeleniumUtils.highLightElement(clickNumber, driver);
	clickNumber.click();
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, recordClick());
	TestUtils.descendingOrder(driver,generator);
	
	SeleniumUtils.presenceOfElement(driver, recordClick());
	SeleniumUtils.highLightElement(firstRecord, driver);
	firstRecord.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuVerify());
	SeleniumUtils.highLightElement(recordName, driver);
	
	SeleniumUtils.highLightElement(ownerName, driver);
	
	SeleniumUtils.highLightElement(record102Verify, driver);
	record102Verify.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuCheck102());
	SeleniumUtils.highLightElement(checkDocu102, driver);
	return new Document102Page(driver);
}


public Document446Page docu446Verify(String methodName, String tcName, ReportGenerator generator) throws Exception {
	
	docuList.click();
	generator.childReport("List View Clicked");
	
	SeleniumUtils.presenceOfElement(driver, listView());
	SeleniumUtils.highLightElement(listBox, driver);
	listBox.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Form no"));
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, clickFrom());
	SeleniumUtils.highLightElement(clickNumber, driver);
	clickNumber.click();
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, recordClick());
	TestUtils.descendingOrder(driver,generator);
	
	SeleniumUtils.presenceOfElement(driver, recordClick());
	SeleniumUtils.highLightElement(firstRecord, driver);
	firstRecord.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuVerify());
	SeleniumUtils.highLightElement(recordName, driver);
	
	SeleniumUtils.highLightElement(ownerName, driver);
	
	SeleniumUtils.highLightElement(record446Verify, driver);
	record446Verify.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuCheck446());
	SeleniumUtils.highLightElement(checkDocu446, driver);
	return new Document446Page(driver);
}


public Document529Page docu529Verify(String methodName, String tcName, ReportGenerator generator) throws Exception {
	
	docuList.click();
	generator.childReport("List View Clicked");
	
	SeleniumUtils.presenceOfElement(driver, listView());
	SeleniumUtils.highLightElement(listBox, driver);
	listBox.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Form no"));
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, clickFrom());
	SeleniumUtils.highLightElement(clickNumber, driver);
	clickNumber.click();
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, recordClick());
	TestUtils.descendingOrder(driver,generator);
	
	SeleniumUtils.presenceOfElement(driver, recordClick());
	SeleniumUtils.highLightElement(firstRecord, driver);
	firstRecord.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuVerify());
	SeleniumUtils.highLightElement(recordName, driver);
	
	SeleniumUtils.highLightElement(ownerName, driver);
	
	SeleniumUtils.highLightElement(record529Verify, driver);
	record529Verify.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuCheck529());
	SeleniumUtils.highLightElement(checkDocu529, driver);
	return new Document529Page(driver);
}


public Document121Page docu121Verify(String methodName, String tcName, ReportGenerator generator) throws Exception {
	
	docuList.click();
	generator.childReport("List View Clicked");
	
	SeleniumUtils.presenceOfElement(driver, listView());
	SeleniumUtils.highLightElement(listBox, driver);
	listBox.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Form no"));
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, clickFrom());
	SeleniumUtils.highLightElement(clickNumber, driver);
	clickNumber.click();
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, recordClick());
	TestUtils.descendingOrder(driver,generator);
	
	SeleniumUtils.presenceOfElement(driver, recordClick());
	SeleniumUtils.highLightElement(firstRecord, driver);
	firstRecord.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuVerify());
	SeleniumUtils.highLightElement(recordName, driver);
	
	SeleniumUtils.highLightElement(ownerName, driver);
	
	SeleniumUtils.highLightElement(record121Verify, driver);
	record121Verify.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuCheck121());
	SeleniumUtils.highLightElement(checkDocu121, driver);
	return new Document121Page(driver);
}

public Document122Page docu122Verify(String methodName, String tcName, ReportGenerator generator) throws Exception {
	
	docuList.click();
	generator.childReport("List View Clicked");
	
	SeleniumUtils.presenceOfElement(driver, listView());
	SeleniumUtils.highLightElement(listBox, driver);
	listBox.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Form no"));
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, clickFrom());
	SeleniumUtils.highLightElement(clickNumber, driver);
	clickNumber.click();
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, recordClick());
	TestUtils.descendingOrder(driver,generator);
	
	SeleniumUtils.presenceOfElement(driver, recordClick());
	SeleniumUtils.highLightElement(firstRecord, driver);
	firstRecord.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuVerify());
	SeleniumUtils.highLightElement(recordName, driver);
	
	SeleniumUtils.highLightElement(ownerName, driver);
	
	SeleniumUtils.highLightElement(record122Verify, driver);
	record122Verify.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuCheck122());
	SeleniumUtils.highLightElement(checkDocu122, driver);
	return new Document122Page(driver);
}

public Document104Page docu104Verify(String methodName, String tcName, ReportGenerator generator) throws Exception {
	
	docuList.click();
	generator.childReport("List View Clicked");
	
	SeleniumUtils.presenceOfElement(driver, listView());
	SeleniumUtils.highLightElement(listBox, driver);
	listBox.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Form no"));
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, clickFrom());
	SeleniumUtils.highLightElement(clickNumber, driver);
	clickNumber.click();
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, recordClick());
	TestUtils.descendingOrder(driver,generator);
	
	SeleniumUtils.presenceOfElement(driver, recordClick());
	SeleniumUtils.highLightElement(firstRecord, driver);
	firstRecord.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuVerify());
	SeleniumUtils.highLightElement(recordName, driver);
	
	SeleniumUtils.highLightElement(ownerName, driver);
	
	SeleniumUtils.highLightElement(record104Verify, driver);
	record104Verify.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuCheck104());
	SeleniumUtils.highLightElement(checkDocu104, driver);
	return new Document104Page(driver);
}


public Document106Page docu106Verify(String methodName, String tcName, ReportGenerator generator) throws Exception {
	
	docuList.click();
	generator.childReport("List View Clicked");
	
	SeleniumUtils.presenceOfElement(driver, listView());
	SeleniumUtils.highLightElement(listBox, driver);
	listBox.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Form no"));
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, clickFrom());
	SeleniumUtils.highLightElement(clickNumber, driver);
	clickNumber.click();
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, recordClick());
	TestUtils.descendingOrder(driver,generator);
	
	SeleniumUtils.presenceOfElement(driver, recordClick());
	SeleniumUtils.highLightElement(firstRecord, driver);
	firstRecord.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuVerify());
	SeleniumUtils.highLightElement(recordName, driver);
	
	SeleniumUtils.highLightElement(ownerName, driver);
	
	SeleniumUtils.highLightElement(record106Verify, driver);
	record106Verify.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuCheck106());
	SeleniumUtils.highLightElement(checkDocu106, driver);
	return new Document106Page(driver);
}


public Document107Page docu107Verify(String methodName, String tcName, ReportGenerator generator) throws Exception {
	
	docuList.click();
	generator.childReport("List View Clicked");
	
	SeleniumUtils.presenceOfElement(driver, listView());
	SeleniumUtils.highLightElement(listBox, driver);
	listBox.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Form no"));
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, clickFrom());
	SeleniumUtils.highLightElement(clickNumber, driver);
	clickNumber.click();
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, recordClick());
	TestUtils.descendingOrder(driver,generator);
	
	SeleniumUtils.presenceOfElement(driver, recordClick());
	SeleniumUtils.highLightElement(firstRecord, driver);
	firstRecord.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuVerify());
	SeleniumUtils.highLightElement(recordName, driver);
	
	SeleniumUtils.highLightElement(ownerName, driver);
	
	SeleniumUtils.highLightElement(record107Verify, driver);
	record107Verify.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuCheck107());
	SeleniumUtils.highLightElement(checkDocu107, driver);
	return new Document107Page(driver);
}


public Document108Page docu108Verify(String methodName, String tcName, ReportGenerator generator) throws Exception {
	
	docuList.click();
	generator.childReport("List View Clicked");
	
	SeleniumUtils.presenceOfElement(driver, listView());
	SeleniumUtils.highLightElement(listBox, driver);
	listBox.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Form no"));
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, clickFrom());
	SeleniumUtils.highLightElement(clickNumber, driver);
	clickNumber.click();
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, recordClick());
	TestUtils.descendingOrder(driver,generator);
	
	SeleniumUtils.presenceOfElement(driver, recordClick());
	SeleniumUtils.highLightElement(firstRecord, driver);
	firstRecord.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuVerify());
	SeleniumUtils.highLightElement(recordName, driver);
	
	SeleniumUtils.highLightElement(ownerName, driver);
	
	SeleniumUtils.highLightElement(record108Verify, driver);
	record108Verify.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuCheck108());
	SeleniumUtils.highLightElement(checkDocu108, driver);
	return new Document108Page(driver);
}


public Document528Page docu528Verify(String methodName, String tcName, ReportGenerator generator) throws Exception {
	
	docuList.click();
	generator.childReport("List View Clicked");
	
	SeleniumUtils.presenceOfElement(driver, listView());
	SeleniumUtils.highLightElement(listBox, driver);
	listBox.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Form no"));
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, clickFrom());
	SeleniumUtils.highLightElement(clickNumber, driver);
	clickNumber.click();
	
	Thread.sleep(5000);
	SeleniumUtils.presenceOfElement(driver, recordClick());
	TestUtils.descendingOrder(driver,generator);
	
	SeleniumUtils.presenceOfElement(driver, recordClick());
	SeleniumUtils.highLightElement(firstRecord, driver);
	firstRecord.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuVerify());
	SeleniumUtils.highLightElement(recordName, driver);
	
	SeleniumUtils.highLightElement(ownerName, driver);
	
	SeleniumUtils.highLightElement(record528Verify, driver);
	record528Verify.click();
	
	SeleniumUtils.presenceOfElement(driver, DocuCheck528());
	SeleniumUtils.highLightElement(checkDocu528, driver);
	return new Document528Page(driver);
}

}
