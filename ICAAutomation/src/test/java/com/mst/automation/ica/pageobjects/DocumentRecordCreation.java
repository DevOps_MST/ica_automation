package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 10/26/2017 
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 10/26/2017 
 * Description: To check permissions for Document Record Creation
 *
 */

public class DocumentRecordCreation extends DriverClass {

	@FindBy(xpath = "//span[text()='Documents']/following::one-tmp-primitive-icon[1]")
	public WebElement documentsTab;

	@FindBy(xpath = "//span[text()='New Document']")
	public WebElement newButton;

	
	public By newButtonPresence() {
		By presenceButton = By.xpath("//span[text()='New Document']");
		return presenceButton;
	}

	@FindBy(xpath = "//h2[text()='New Document']")
	public WebElement popUp_verify;
	
	public By newDocumentPresence() {
		By presenceDocu = By.xpath("//h2[text()='New Document']");
		return presenceDocu;
	}

	@FindBy(xpath = "//span[text()='Information']")
	public WebElement documentInfo;
	
	public By documentInfoPresence() {
		By presenceInfo = By.xpath("//span[text()='Information']");
		return presenceInfo;
	}

	@FindBy(xpath = "//span[contains(.,'Document Name')]/following::input[1]")
	public WebElement documentName;

	@FindBy(xpath = "//span[text()='Today']")
	public WebElement todayDOI;

	@FindBy(xpath = "//span[contains(.,'First Name')]/following::input[1]")
	public WebElement firstName;

	@FindBy(xpath = "//span[contains(.,'Last Name')]/following::input[1]")
	public WebElement lastName;

	@FindBy(xpath = "//span[contains(.,'DOB')]/following::a[1]")
	public WebElement clickDOB;

	@FindBy(xpath = "//span[text()='Today']")
	public WebElement todayDOB;

	@FindBy(css = "button[title='Save']")
	public WebElement clickSave;

	public By savePresence() {
		By presenceSave = By.cssSelector("button[title='Save']");
		return presenceSave;
	}

	@FindBy(xpath = "//span[contains(.,'Home')]")
	public WebElement clickHome;

	
	private By presenceHome;
	
	public By homePresence() {
		presenceHome  = By.xpath("//span[contains(.,'Home')]");
		return presenceHome;
	}
	
	public DocumentRecordCreation(WebDriver driver) {
		super(driver);
	}

	
	/*Actions that to check the permission sets for that appropriate profile*/
	
	public void createDocument(String methodName, String tcName, ReportGenerator generator) throws Exception {

		documentsTab.click();
		generator.childReport("Document tab clicked");
		SeleniumUtils.presenceOfElement(driver, newButtonPresence());
		newButton.click();
		generator.childReport("New button clicked");
		SeleniumUtils.presenceOfElement(driver, newDocumentPresence());
		SeleniumUtils.highLightElement(popUp_verify, driver);
		generator.childReport("Next clicked");
		SeleniumUtils.presenceOfElement(driver, documentInfoPresence());
		SeleniumUtils.highLightElement(documentInfo, driver);
		documentName.click();

	}

}
