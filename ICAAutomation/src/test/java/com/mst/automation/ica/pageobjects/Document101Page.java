package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class Document101Page extends DriverClass{
	

	@FindBy(xpath = ".//span[text()=\"First Name\"]/following::span[1]/span")
	public WebElement claimantFirstName;

	@FindBy(xpath = ".//span[text()='Last Name']/following::span[1]/span")
	public WebElement claimantLastName;

	@FindBy(xpath = ".//span[text()='Middle Initial']/following::span[1]/span")
	public WebElement claimantMiddleName;

	@FindBy(xpath = ".//span[text()='Social Security Number']/following::span[4]/span")
	public WebElement socialNumber;

	@FindBy(xpath = ".//span[text()='Home Address']/following::span[1]/span")
	public WebElement homeAddress;

	@FindBy(xpath = ".//span[text()='Date of Birth']/preceding::span[7]/span")
	public WebElement City;

	@FindBy(xpath = ".//span[text()='Date of Birth']/following::span[12]/span")
	public WebElement ZipCode;

	@FindBy(xpath = ".//span[text()='Date of Birth']/following::span[1]/span")
	public WebElement DOB;

	@FindBy(xpath = ".//span[text()='Date of Birth']/following::span[23][text()='State']/following::span[1]/span")
	public WebElement State;

	@FindBy(xpath = ".//span[text()='Date of Birth']/following::span[31]/span")
	public WebElement Telephone;

	@FindBy(xpath = ".//span[text()='Date of Birth']/following::span[38]/span")
	public WebElement Maritalstatus;

	@FindBy(xpath = ".//span[text()='Date of Birth']/following::span[46]/span")
	public WebElement MatchedClaim;

	//Employer info...
	@FindBy(xpath = ".//span[text()='Office Address']/preceding::span[14]/span")
	public WebElement EmployerName;

	@FindBy(xpath = ".//span[text()='Office Address']/following::span[1]/span")
	public WebElement EmployerOfficeAddress;

	@FindBy(xpath = ".//span[text()='Office Address']/following::span[9]/span")
	public WebElement EmployerState;

	@FindBy(xpath = ".//span[text()='Office Address']/following::span[17]/span")
	public WebElement EmployerTelephone;

	@FindBy(xpath = ".//span[text()='Office Address']/following::span[5]/span")
	public WebElement EmployerCity;

	@FindBy(xpath = ".//span[text()='Office Address']/following::span[13]/span")
	public WebElement EmployerZipcode;

	@FindBy(xpath = ".//span[text()='Office Address']/following::span[24]/span")
	public WebElement DoI;

	//Accident Info..
	@FindBy(xpath = ".//span[text()='Time Claimant began work']/following::span[1]/span")
	public WebElement TimeClaimantBW;

	@FindBy(xpath = ".//span[text()='Time Claimant began work']/following::span[5]/span")
	public WebElement TimeOfEvent;

	@FindBy(xpath = ".//span[text()='Time Claimant began work']/following::span[9]/span")
	public WebElement LDOW;

	@FindBy(xpath = ".//span[text()='Time Claimant began work']/following::span[13]/span")
	public WebElement DOENInjuiry;

	@FindBy(xpath = ".//span[text()='Time Claimant began work']/following::span[25]/span")
	public WebElement Payrollreport;

	@FindBy(xpath = ".//span[text()='Time Claimant began work']/following::span[32]/span")
	public WebElement JobTitle;

	@FindBy(xpath = ".//span[text()='Time Claimant began work']/following::span[36]/span")
	public WebElement DepartmentNumber;

	@FindBy(xpath = ".//span[text()='Time Claimant began work']/following::span[40]/span")
	public WebElement AssignedDepartment;

	@FindBy(xpath = ".//span[text()='Time Claimant began work']/following::span[48]/span")
	public WebElement EmployerPermises;

	@FindBy(xpath = ".//span[text()='Time Claimant began work']/following::span[87]/span")
	public WebElement InjuryOrIll;

	@FindBy(xpath = ".//span[text()='Time Claimant began work']/following::span[80]/span")
	public WebElement BodyInjuired;

	@FindBy(xpath = ".//span[text()='Time Claimant began work']/following::span[98]/span")
	public WebElement Fatal;

	@FindBy(xpath = ".//span[text()='Time Claimant began work']/following::span[112]/span")
	public WebElement EmergencyRoom;

	@FindBy(xpath = ".//span[text()='Time Claimant began work']/following::span[139]/span")
	public WebElement Overnight;

	@FindBy(xpath = ".//span[text()='Time Claimant began work']/following::span[163]/span")
	public WebElement ClaimDoubt;

	//Cause of incident
	@FindBy(xpath = ".//span[text()='Cause of Accident']/following::span[5]/span")
	public WebElement WhatHappend;

	@FindBy(xpath = ".//span[text()='Cause of Accident']/following::span[12]/span")
	public WebElement HarmentClmnt;

	@FindBy(xpath = ".//span[text()='Cause of Accident']/following::span[19]/span")
	public WebElement BeforeInjuired;

	// Claimant's Wage Data
	@FindBy(xpath = ".//span[contains(text(),'Wage Data')]/following::span[2]/span")
	public WebElement EmployInjuired;

	@FindBy(xpath = ".//span[contains(text(),'Wage Data')]/following::span[9]/span")
	public WebElement ClmntHPDFrom;

	@FindBy(xpath = ".//span[contains(text(),'Wage Data')]/following::span[16]/span")
	public WebElement OvertimeWhenInjuired;

	@FindBy(xpath = ".//span[contains(text(),'Wage Data')]/following::span[23]/span")
	public WebElement ClmtHPDTo;

	@FindBy(xpath = ".//span[contains(text(),'Wage Data')]/following::span[30]/span")
	public WebElement CoUsuallyWorked;

	@FindBy(xpath = ".//span[contains(text(),'Wage Data')]/following::span[37]/span")
	public WebElement ClmtUsuallyWorked;

	//Other info
	@FindBy(xpath = ".//span[text()='Other Information']/following::span[5]/span")
	public WebElement WorkLossExecpted;

	@FindBy(xpath = ".//span[text()='Other Information']/following::span[9]/span")
	public WebElement DOLH;

	@FindBy(xpath = ".//span[text()='Other Information']/following::span[13]/span")
	public WebElement PDOI;

	@FindBy(xpath = ".//span[text()='Other Information']/following::span[20]/span")
	public WebElement PermEmployee;

	@FindBy(xpath = ".//span[text()='Other Information']/following::span[34]/span")
	public WebElement StatusASAppli;

	@FindBy(xpath = ".//span[text()='Other Information']/following::span[57]/span")
	public WebElement ClaimDependencies;

	@FindBy(xpath = ".//span[text()='Other Information']/following::span[64]/span")
	public WebElement MoSalary;

	//Signature info
	@FindBy(xpath = ".//span[text()='Signature Info']/following::span[14]/span")
	public WebElement SignInfoEmail;

	

	
	public By listView() {
		By list = By.cssSelector("input[data-aura-class=\"uiInput uiInputTextForAutocomplete uiInput--default uiInput--input\"]");
		return list;
	}
	
	/*To verify matched to claim verify*/
	
	public By match() {
		By claim = By.xpath("//span[text()='Matched to Claim?']/following::span[2]");
		return claim;
	}
	
	@FindBy(xpath = "//span[text()='Matched to Claim?']/following::span[2]")
	public WebElement matchedToclaim;
	
	@FindBy(xpath = "//span[text()='Matched to Claim?']/following::span[3]")
	public WebElement matchedToEdit;
	
	public By saveButton() {
		By save = By.cssSelector("button[title=\"Save\"]");
		return save;
	}
	
	@FindBy(xpath = "//span[text()='Matched to Claim?']/following::span[3]")
	public WebElement saveClick;
	
	@FindBy(xpath = "//span/span[text()='Matched to Claim?']/following::a[1]")
	public WebElement editField;
	
	public By relatedtVerify() {
		By related = By.xpath("//p[text()='101']/following::a[6]/span[2]");
		return related;
	}
	
	
	@FindBy(xpath = "//p[text()='101']/following::a[6]/span[2]")
	public WebElement relatedTab;
	
	public By documentVerify() {
		By docu = By.xpath("//h2/a/span[text()='Documents']");
		return docu;
	}
	
	@FindBy(xpath = "//h2/a/span[text()='Documents']")
	public WebElement documenthighlight;
	
	public By dropbeforeVerify() {
		By down = By.xpath("//h2/a/span[text()='Documents']/following::a[3]");
		return down;
	}
	
	
	@FindBy(xpath = "//h2/a/span[text()='Documents']/following::a[3]")
	public WebElement dropdownbefore;
	
	public By dropafterVerify() {
		By down = By.xpath("//h2/a/span[text()='Documents']/following::a[4]");
		return down;
	}
	
	
	@FindBy(xpath = "//h2/a/span[text()='Documents']/following::a[4]")
	public WebElement dropdownafter;
	
	public By clickEdit() {
		By click = By.cssSelector("a[title=\"Edit\"]");
		return click;
	}
	
	@FindBy(css = "a[title=\"Edit\"]")
	public WebElement editCLick;
	
	public By pageEdit() {
		By edit = By.xpath("//h2[contains(.,'Edit ')]");
		return edit;
	}
	
	@FindBy(xpath = "//h2[contains(.,'Edit ')]")
	public WebElement editPage;
	
	@FindBy(xpath = "//label/span[text()='Claim']/following::input[1]")
	public WebElement newClaimNumber;
	
	public By firstReocrd() {
		By first = By.xpath("//label/span[text()='Claim']/following::a[1]");
		return first;
	}
	
	@FindBy(xpath = "//label/span[text()='Claim']/following::a[1]")
	public WebElement firstRecordClick;
	
	@FindBy(xpath = "//h2[contains(.,'Edit ')]/following::button[4]")
	public WebElement saveclickEdit;
	
	public By claimRecord() {
		By rec = By.xpath("//table/tbody/tr/td[1]/a");
		return rec;
	}
	
	@FindBy(xpath = "//table/tbody/tr/td[1]/a")
	public WebElement saveclaim;
	
	public By detailVerify() {
		By det = By.xpath("//table/tbody/tr/td[1]/a");
		return det;
	}
	
	
	@FindBy(xpath = "//p[text()='101']/following::a[5]/span[2]")
	public WebElement detailTab;
	
	@FindBy(xpath = "//label/span[text()='Claim']/following::a[3]/span[1]")
	public WebElement deleteClaim;
	
	
	
	
	public Document101Page(WebDriver driver) {
		super(driver);
	}
	
	public void verify101Document(String methodName, String tcName, ReportGenerator generator) throws Exception{
			
		generator.childReport("Claimant first name verified");
		String fName = claimantFirstName.getText();
		TestUtils.compareText(fName, GoogleSheetAPI.ReadData(methodName, tcName, "claimant first name"), claimantFirstName, driver); 
		generator.childReport("Claimant first name verified");
		String lName = claimantLastName.getText();
		TestUtils.compareText(lName, GoogleSheetAPI.ReadData(methodName, tcName, "claimant last name"), claimantLastName, driver); 
		generator.childReport("Claimant first name verified");
		String mi = claimantMiddleName.getText();
		TestUtils.compareText(mi, GoogleSheetAPI.ReadData(methodName, tcName, "claimant middle initial"), claimantMiddleName, driver); 
		generator.childReport("Claimant first name verified");
		String ssn = socialNumber.getText();
		TestUtils.compareText(ssn, GoogleSheetAPI.ReadData(methodName, tcName, "claimant social security number"), socialNumber, driver); 
		generator.childReport("Claimant zipcode verified");
		SeleniumUtils.scrollDown(driver);
		String zipcode = ZipCode.getText();
		TestUtils.compareText(zipcode, GoogleSheetAPI.ReadData(methodName, tcName, "claimant zip code"), ZipCode, driver); 
		SeleniumUtils.scrollDown(driver);
		generator.childReport("Claimant city verified");
		String city = City.getText();
		TestUtils.compareText(city, GoogleSheetAPI.ReadData(methodName, tcName, "claimant city"), City, driver); 
		generator.childReport("Claimant state verified");
		String state = State.getText();
		TestUtils.compareText(state, GoogleSheetAPI.ReadData(methodName, tcName, "claimant state"), State, driver); 
		generator.childReport("Employer's Name verified");
		String employerName = EmployerName.getText();
		TestUtils.compareText(employerName, GoogleSheetAPI.ReadData(methodName, tcName, "employer"), EmployerName, driver);
		generator.childReport("State verified");
		SeleniumUtils.scrollDown(driver);
		String employerState = EmployerState.getText();
		TestUtils.compareText(employerState, GoogleSheetAPI.ReadData(methodName, tcName, "employerState"), EmployerState, driver); 
		generator.childReport("Telephone verified");
		 
		String employerTelephone = EmployerTelephone.getText();
		TestUtils.compareText(employerTelephone, GoogleSheetAPI.ReadData(methodName, tcName, "employerTelephoneAct"), EmployerTelephone, driver); 
		generator.childReport("City verified");
		String employerCity = EmployerCity.getText();
		TestUtils.compareText(employerCity, GoogleSheetAPI.ReadData(methodName, tcName, "employerCity"), EmployerCity, driver); 
		generator.childReport("Zip Code verified");
		SeleniumUtils.scrollDown(driver);
		 
		String employerZipcode = EmployerZipcode.getText();
		TestUtils.compareText(employerZipcode, GoogleSheetAPI.ReadData(methodName, tcName, "employerZipCode"), EmployerZipcode, driver); 
		SeleniumUtils.scrollDown(driver);
		generator.childReport("Class Code on Payroll Report verified");
		
		String classCodeonPayroll = Payrollreport.getText();
		TestUtils.compareText(classCodeonPayroll, GoogleSheetAPI.ReadData(methodName, tcName, "classCodeonPayroll"), Payrollreport, driver); 
		generator.childReport("*Claimant's Job Title When Injured verified");
		String jobTitle = JobTitle.getText();
		TestUtils.compareText(jobTitle, GoogleSheetAPI.ReadData(methodName, tcName, "claimantJobTitle"), JobTitle, driver); 
		generator.childReport("*Department Number verified");
		SeleniumUtils.scrollDown(driver);
		String deptNumber = DepartmentNumber.getText();
		TestUtils.compareText(deptNumber, GoogleSheetAPI.ReadData(methodName, tcName, "departmentNumber"), DepartmentNumber, driver);
		generator.childReport("Claimant's Assigned Department verified");
		String assignedDept= AssignedDepartment.getText();
		TestUtils.compareText(assignedDept, GoogleSheetAPI.ReadData(methodName, tcName, "claimantAssignedDepartment"), AssignedDepartment, driver);
		generator.childReport("What was the Injury or Illness? verified");
		String whatWasTheInjury= InjuryOrIll.getText();
		TestUtils.compareText(whatWasTheInjury, GoogleSheetAPI.ReadData(methodName, tcName, "whatWasTheInjury"), InjuryOrIll, driver);
		SeleniumUtils.scrollDown(driver);
		generator.childReport("Claimant first name verified");
		String bodyInjuired= BodyInjuired.getText();
		TestUtils.compareText(bodyInjuired, GoogleSheetAPI.ReadData(methodName, tcName, "partOfBodyInjured"), BodyInjuired, driver); 
		generator.childReport("What Happened? verified");
		String whatHappend= WhatHappend.getText();
		TestUtils.compareText(whatHappend, GoogleSheetAPI.ReadData(methodName, tcName, "whatHappened"), WhatHappend, driver); 
		generator.childReport("Object/Substance That Harmed the Clmnt? verified");
		SeleniumUtils.scrollDown(driver);
		 
		String harmentClmnt= HarmentClmnt.getText();
		TestUtils.compareText(harmentClmnt, GoogleSheetAPI.ReadData(methodName, tcName, "objectSubstance"), HarmentClmnt, driver); 
		generator.childReport("What Was Clmt Doing Before Incident? verified");
		 
		String beforeInjuired= BeforeInjuired.getText();
		TestUtils.compareText(beforeInjuired, GoogleSheetAPI.ReadData(methodName, tcName, "whatWasClmtDoingBeforeIncident"), BeforeInjuired, driver); 
		generator.childReport("Hours Per Day Clmt Worked (From) verified");
		String clmntHPDFrom= ClmntHPDFrom.getText();
		TestUtils.compareText(clmntHPDFrom, GoogleSheetAPI.ReadData(methodName, tcName, "hoursPerDayClmtWorkedFrom"), ClmntHPDFrom, driver); 
		generator.childReport("Hours Per Day Clmt Worked (To) verified");
		String clmtHPDTo= ClmtHPDTo.getText();
		TestUtils.compareText(clmtHPDTo, GoogleSheetAPI.ReadData(methodName, tcName, "hoursPerDayClmtWorkedTo"), ClmtHPDTo, driver);
		generator.childReport("# Days Per Week Usually Worked?(Co.) verified");
		SeleniumUtils.scrollDown(driver);
		 
		String coUsuallyWorked= CoUsuallyWorked.getText();
		TestUtils.compareText(coUsuallyWorked, GoogleSheetAPI.ReadData(methodName, tcName, "daysPerWeekUsuallyWorkedCo"), CoUsuallyWorked, driver);
		generator.childReport("# Days Per Week Usually Worked?(Clmnt.) verified");
		SeleniumUtils.scrollDown(driver);
		 
		String clmtUsuallyWorked= ClmtUsuallyWorked.getText();
		TestUtils.compareText(clmtUsuallyWorked, GoogleSheetAPI.ReadData(methodName, tcName, "daysPerWeekUsuallyWorkedClmt"), ClmtUsuallyWorked, driver);
		SeleniumUtils.scrollDown(driver);
		generator.childReport("Email Verified");
		String submitterEmailAddress= SignInfoEmail.getText();
		TestUtils.compareText(submitterEmailAddress, GoogleSheetAPI.ReadData(methodName, tcName, "submitterEmailAddress"), SignInfoEmail, driver);

		
	}
	public void matchClaim101form(String methodName, String tcName, ReportGenerator generator) throws Exception{
		
		SeleniumUtils.presenceOfElement(driver, match());
		
		generator.childReport("Matched to claim 'No' found");
		String no = matchedToclaim.getText();
		TestUtils.compareText(no, GoogleSheetAPI.ReadData(methodName, tcName, "matched No"), matchedToclaim, driver);
		
		SeleniumUtils.presenceOfElement(driver, relatedtVerify());
		SeleniumUtils.elementToBeClickable(driver, relatedtVerify());
		SeleniumUtils.scrollup(driver);
		SeleniumUtils.highLightElement(relatedTab, driver);
		SeleniumUtils.scrollup(driver);
		generator.childReport("Related Tab Clicked");
		relatedTab.click();
		
		SeleniumUtils.presenceOfElement(driver, documentVerify());
		SeleniumUtils.highLightElement(documenthighlight, driver);
		
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, dropbeforeVerify());
		SeleniumUtils.highLightElement(dropdownbefore, driver);
		generator.childReport("Edit Clicked");
		dropdownbefore.click();
		
		SeleniumUtils.presenceOfElement(driver, clickEdit());
		editCLick.click();
		
		SeleniumUtils.presenceOfElement(driver, pageEdit());
		SeleniumUtils.highLightElement(editPage, driver);
		
		generator.childReport("Claim Number Entered");
		newClaimNumber.click();
		
		SeleniumUtils.presenceOfElement(driver, firstReocrd());
		SeleniumUtils.highLightElement(firstRecordClick, driver);
		generator.childReport("First Record Clicked");
		firstRecordClick.click();
		
		generator.childReport("Save button Clicked");
		saveclickEdit.click();
		
		/*SeleniumUtils.presenceOfElement(driver, claimRecord());
		SeleniumUtils.highLightElement(saveclaim, driver);*/
		
		
		SeleniumUtils.presenceOfElement(driver, detailVerify());
		SeleniumUtils.elementToBeClickable(driver, detailVerify());
		SeleniumUtils.highLightElement(detailTab, driver);
		generator.childReport("Detail Tab Clicked");
		detailTab.click();
		Thread.sleep(5000);
		generator.childReport("Record Refreshed");
		driver.navigate().refresh();
		Thread.sleep(5000);
		
		SeleniumUtils.presenceOfElement(driver, match());
		driver.navigate().refresh();
		Thread.sleep(5000);
		generator.childReport("Matched Changed as 'Yes' Verified");
		String yes = matchedToclaim.getText();
		TestUtils.compareText(yes, GoogleSheetAPI.ReadData(methodName, tcName, "matched Yes"), matchedToclaim, driver);
		
		SeleniumUtils.presenceOfElement(driver, relatedtVerify());
		SeleniumUtils.elementToBeClickable(driver, relatedtVerify());
		SeleniumUtils.scrollup(driver);
		SeleniumUtils.highLightElement(relatedTab, driver);
		SeleniumUtils.scrollup(driver);
		generator.childReport("Related Tab Clicked");
		relatedTab.click();
		
		SeleniumUtils.presenceOfElement(driver, documentVerify());
		SeleniumUtils.highLightElement(documenthighlight, driver);
		
		Thread.sleep(5000);
		SeleniumUtils.presenceOfElement(driver, dropafterVerify());
		SeleniumUtils.highLightElement(dropdownafter, driver);
		generator.childReport("Edit Clicked");
		dropdownafter.click();
		
		SeleniumUtils.presenceOfElement(driver, clickEdit());
		editCLick.click();
		
		SeleniumUtils.presenceOfElement(driver, pageEdit());
		SeleniumUtils.highLightElement(editPage, driver);
		
		generator.childReport("Claim Number Deleted");
		deleteClaim.click();
		
		generator.childReport("Save Clicked");
		saveclickEdit.click();
		
/*		SeleniumUtils.presenceOfElement(driver, claimRecord());
		SeleniumUtils.highLightElement(saveclaim, driver);*/
		
		SeleniumUtils.presenceOfElement(driver, detailVerify());
		SeleniumUtils.elementToBeClickable(driver, detailVerify());
		SeleniumUtils.highLightElement(detailTab, driver);
		Thread.sleep(5000);
		generator.childReport("Detail Tab Clicked");
		detailTab.click();
		
		
		driver.navigate().refresh();
		Thread.sleep(5000);
		
		SeleniumUtils.presenceOfElement(driver, match());
		driver.navigate().refresh();
		Thread.sleep(5000);
		generator.childReport("Matched to claim 'No' verified");
		String noagain = matchedToclaim.getText();
		TestUtils.compareText(noagain, GoogleSheetAPI.ReadData(methodName, tcName, "matched No"), matchedToclaim, driver);

	
	}
	
}
