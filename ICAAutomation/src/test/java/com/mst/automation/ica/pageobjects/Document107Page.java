package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class Document107Page extends DriverClass {
	
	public By listView() {
		By list = By.cssSelector("input[data-aura-class=\"uiInput uiInputTextForAutocomplete uiInput--default uiInput--input\"]");
		return list;
	}

	public Document107Page(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath = "//div[1]/span[text()='ICA Claim No.']/following::span[2]")
	public WebElement icaClaimNo;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant First Name']/following::span[2]")
	public WebElement claimantFirstName;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Last Name']/following::span[2]")
	public WebElement claimantLastName;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Last Name']/following::span[14]")
	public WebElement claimantStreet;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Last Name']/following::span[18]")
	public WebElement claimantState;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Last Name']/following::span[22]")
	public WebElement claimantCity;
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant Last Name']/following::span[26]")
	public WebElement claimantZipcode;
	
	@FindBy(xpath = "//h3/button/span[text()='Carrier or Self-Insured Details']/following::span[3]")
	public WebElement carrierName;
	
	@FindBy(xpath = "//h3/button/span[text()='Carrier or Self-Insured Details']/following::span[7]")
	public WebElement carrierState;
	
	@FindBy(xpath = "//h3/button/span[text()='Carrier or Self-Insured Details']/following::span[11]")
	public WebElement carrierStreet;
	
	@FindBy(xpath = "//h3/button/span[text()='Carrier or Self-Insured Details']/following::span[15]")
	public WebElement carrierZipcode;
	
	@FindBy(xpath = "//h3/button/span[text()='Carrier or Self-Insured Details']/following::span[19]")
	public WebElement carrierCity;
	
	@FindBy(xpath = "//h3/button/span[text()='Authentication Details']/following::span[11]")
	public WebElement authenticationBy;
	
	
	
	
public void verify107Document(String methodName, String tcName, ReportGenerator generator) throws Exception{
		
		SeleniumUtils.highLightElement(icaClaimNo, driver);
		String icaClaim = icaClaimNo.getText();
		TestUtils.compareText(icaClaim, GoogleSheetAPI.ReadData(methodName, tcName, "ICA Claim No"), icaClaimNo, driver);
		
		SeleniumUtils.highLightElement(claimantFirstName, driver);
		String clmntFrstName = claimantFirstName.getText();
		TestUtils.compareText(clmntFrstName, GoogleSheetAPI.ReadData(methodName, tcName, "Claimant First Name"), claimantFirstName, driver);
		
		SeleniumUtils.highLightElement(claimantLastName, driver);
		String clmntLstName = claimantLastName.getText();
		TestUtils.compareText(clmntLstName, GoogleSheetAPI.ReadData(methodName, tcName, "Claimant Last Name"), claimantLastName, driver);
		
		SeleniumUtils.highLightElement(claimantStreet, driver);
		String clmntStrt = claimantStreet.getText();
		TestUtils.compareText(clmntStrt, GoogleSheetAPI.ReadData(methodName, tcName, "Claimant Street"), claimantStreet, driver);
		
		SeleniumUtils.highLightElement(claimantState, driver);
		String clmntStat = claimantState.getText();
		TestUtils.compareText(clmntStat, GoogleSheetAPI.ReadData(methodName, tcName, "Claimant State"), claimantState, driver);
		
		SeleniumUtils.highLightElement(claimantCity, driver);
		String clmntCty = claimantCity.getText();
		TestUtils.compareText(clmntCty, GoogleSheetAPI.ReadData(methodName, tcName, "Claimant City"), claimantCity, driver);
		
		SeleniumUtils.highLightElement(claimantZipcode, driver);
		String clmntZip = claimantZipcode.getText();
		TestUtils.compareText(clmntZip, GoogleSheetAPI.ReadData(methodName, tcName, "Claimant Zipcode"), claimantZipcode, driver);
		
		SeleniumUtils.highLightElement(carrierName, driver);
		String carrName = carrierName.getText();
		TestUtils.compareText(carrName, GoogleSheetAPI.ReadData(methodName, tcName, "Carrier Name"), carrierName, driver);
		
		SeleniumUtils.highLightElement(carrierState, driver);
		String carrStat = carrierState.getText();
		TestUtils.compareText(carrStat, GoogleSheetAPI.ReadData(methodName, tcName, "Carrier State"), carrierState, driver);
		
		SeleniumUtils.highLightElement(carrierStreet, driver);
		String carrStrt = carrierStreet.getText();
		TestUtils.compareText(carrStrt, GoogleSheetAPI.ReadData(methodName, tcName, "Carrier Street"), carrierStreet, driver);
		
		SeleniumUtils.highLightElement(carrierZipcode, driver);
		String carrZip = carrierZipcode.getText();
		TestUtils.compareText(carrZip, GoogleSheetAPI.ReadData(methodName, tcName, "Carrier Zipcode"), carrierZipcode, driver);
		
		SeleniumUtils.highLightElement(carrierCity, driver);
		String carrCty = carrierCity.getText();
		TestUtils.compareText(carrCty, GoogleSheetAPI.ReadData(methodName, tcName, "Carrier City"), carrierCity, driver);
		
		SeleniumUtils.highLightElement(authenticationBy, driver);
		String authBy = authenticationBy.getText();
		TestUtils.compareText(authBy, GoogleSheetAPI.ReadData(methodName, tcName, "Mailed By"), authenticationBy, driver);
		
		
		
		
}

}
