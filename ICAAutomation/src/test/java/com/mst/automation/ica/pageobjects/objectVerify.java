/**
 * 
 */
package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.CommonUtils;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

/**
 * @author Aartheeswaran
 * Created date: Jan 17, 2018
 * Last Edited by: Aartheeswaran...
 * Last Edited date: 
 * Description: 
 *
 */
public class objectVerify extends DriverClass{
	
	@FindBy(xpath = "//h2[text()='App Launcher']")
	public WebElement appLauncher_Verify;
	
	public By obj() {
		By manager = By.xpath("//h2[text()='App Launcher']");
		return manager;
	}
	
	public By verify() {
		By result = By.xpath("//span[text()='101']");
		return result;
	}
	
	@FindBy(xpath = "//span[text()='101']")
	public WebElement Document101;
	
	@FindBy(xpath = "//span[text()='102']")
	public WebElement Document102;
	
	@FindBy(xpath = "//span[text()='104']")
	public WebElement Document104;
	
	@FindBy(xpath = "//span[text()='106']")
	public WebElement Document106;
	
	@FindBy(xpath = "//span[text()='107']")
	public WebElement Document107;
	
	@FindBy(xpath = "//span[text()='108']")
	public WebElement Document108;
	
	@FindBy(xpath = "//span[text()='120']")
	public WebElement Document120;
	
	@FindBy(xpath = "//span[text()='121']")
	public WebElement Document121;
	
	@FindBy(xpath = "//span[text()='122']")
	public WebElement Document122;
	
	@FindBy(xpath = "//span[text()='407']")
	public WebElement Document407;
	
	@FindBy(xpath = "//span[text()='446']")
	public WebElement Document446;
	
	@FindBy(xpath = "//span[text()='528']")
	public WebElement Document528;
	
	@FindBy(xpath = "//span[text()='529']")
	public WebElement Document529;
	
	@FindBy(xpath = "//span[text()='ADOSH']")
	public WebElement adosh;
	
	@FindBy(xpath = "//span[text()='Accounts']")
	public WebElement accounts;
	
	@FindBy(xpath = "//span[text()='Carriers']")
	public WebElement carriers;
	
	@FindBy(xpath = "//li/a/span/span[text()='Claim Tasks']")
	public WebElement claimtasks;
	
	@FindBy(xpath = "//li/a/span/span[text()='Claims']")
	public WebElement claims;
	
	@FindBy(xpath = "//li/a/span/span[text()='Contacts']")
	public WebElement contacts;
	
	@FindBy(xpath = "//li/a/span/span[text()='Document Recipients']")
	public WebElement documentrecipients;
	
	@FindBy(xpath = "//li/a/span/span[text()='Outbound Communications']")
	public WebElement notificationchannel;
	
	@FindBy(xpath = "//li/a/span/span[text()='Other Documents']")
	public WebElement otherdocuments;
	
	
	public objectVerify(WebDriver driver) {
		super(driver);
	}
	
	public void verifyObjectAll(String methodName, String tcName, ReportGenerator generator)
			throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, obj());
		SeleniumUtils.highLightElement(appLauncher_Verify, driver);
		
		SeleniumUtils.presenceOfElement(driver, verify());
		
		SeleniumUtils.highLightElement(Document101, driver);
		SeleniumUtils.highLightElement(Document102, driver);
		SeleniumUtils.highLightElement(Document104, driver);
		SeleniumUtils.highLightElement(Document106, driver);
		SeleniumUtils.highLightElement(Document107, driver);
		SeleniumUtils.highLightElement(Document108, driver);
		SeleniumUtils.highLightElement(Document120, driver);
		SeleniumUtils.highLightElement(Document121, driver);
		SeleniumUtils.highLightElement(Document122, driver);
		SeleniumUtils.highLightElement(Document407, driver);
		SeleniumUtils.highLightElement(Document446, driver);
		SeleniumUtils.highLightElement(Document528, driver);
		SeleniumUtils.highLightElement(Document529, driver);
		SeleniumUtils.highLightElement(adosh, driver);
		SeleniumUtils.highLightElement(accounts, driver);
		SeleniumUtils.highLightElement(claimtasks, driver);
		SeleniumUtils.highLightElement(claims, driver);
		SeleniumUtils.highLightElement(contacts, driver);
		SeleniumUtils.highLightElement(documentrecipients, driver);
		SeleniumUtils.highLightElement(notificationchannel, driver);
		SeleniumUtils.highLightElement(otherdocuments, driver);
		
}
}
