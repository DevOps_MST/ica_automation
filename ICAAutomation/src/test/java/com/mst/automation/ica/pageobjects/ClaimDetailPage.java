/**
 * @author Ashok Kumar Ganesan
 * Created date:
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: 
 */
package com.mst.automation.ica.pageobjects;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.customexception.CustomException;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.CommonUtils;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

/**
 * @author Ashok Kumar Ganesan
 * Created date: Feb 10, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: This page contains the page objects and the methods of Claim Detail Page
 */
public class ClaimDetailPage extends DriverClass {

	@FindBy(xpath="//span[text()='Claim Status']/following::span[1]")
	public WebElement claimStatus;
	
	@FindBy(xpath="//span[text()='Owner']/following::force-lookup[1]/span")
	public WebElement owner;
	
	@FindBy(xpath="//span[text()='Claimant Account']/preceding::span[2]")
	public WebElement firstName;
	
	@FindBy(xpath="//span[text()='Shipping Address']/following::span[1]/a/div[@class='slds-truncate forceOutputAddressText']")
	public WebElement shippingAddress;	
		
	@FindBy(xpath="//span[text()='Claimant Account']/following::span[1]")
	public WebElement claimantAccount;
	
	@FindBy(xpath="//div[@class='windowViewMode-normal oneContent active forcePageHost']//../span[text()='Claimant Account']/following::span[1]/div/a")
	public WebElement claimantAccountPage;
	
	@FindBy(xpath="//span[text()='Claimant Account']/following::div[7]/span/span")
	public WebElement lastName;
	
	@FindBy(xpath="//span[text()='Sex']/following::span[1]")
	public WebElement sex;
	
	@FindBy(xpath="//span[text()='SSN']/following::span[1]/span")
	public WebElement claimSSN;
	
	@FindBy(xpath="//span[text()='Delivery Channel']/following::div[30]/span/span")
	public WebElement maritalStatus;
	
	@FindBy(xpath="//div[@title='Delete']")
	public WebElement deleteLink;
	
	@FindBy(xpath="//button[@title='Delete']")
	public WebElement deleteButton;
	
	@FindBy(xpath="//div[@class='windowViewMode-normal oneContent active forcePageHost']//../span[text()='Employer Account']/following::span[1]/div/a")
	public WebElement employerAccount;
	
	@FindBy(xpath="//div[@class='windowViewMode-normal oneContent active forcePageHost']//../span[text()='Tasks']")
    public WebElement tasks;
    
    @FindBy(xpath="//div[@class='windowViewMode-normal oneContent active forcePageHost']//./table//tr[1]//a[1]")
    public WebElement tasksClick;
    
    @FindBy(xpath="//p[text()='Claim']/following::span[1]")
    public WebElement claimNumberVerify;
    
    @FindBy(xpath="//div[1]/span[text()='Carrier Account']/following::a[1]")
    public WebElement carriernameExp;
    
    @FindBy(xpath="//div[1]/span[text()='Policy No.']/following::span[2]")
    public WebElement policyNoVerify; 
    
    @FindBy(xpath="//h2[starts-with(.,'Edit ')]/following::span[text()='First Name']/following::input[1]")
    public WebElement editFirstName;
    
    @FindBy(xpath="//h2[starts-with(.,'Edit ')]/following::span[text()='Last Name']/following::input[1]")
    public WebElement editLastName;
    
    @FindBy(xpath="//h2[starts-with(.,'Edit ')]/following::span[text()='DOB']/following::input[1]")
    public WebElement editDOB;
    
    @FindBy(xpath="//h2[starts-with(.,'Edit ')]/following::span[text()='SSN']/following::input[1]")
    public WebElement editSSN;
    
    @FindBy(xpath="//h2[starts-with(.,'Edit ')]/following::span[text()='First Name']/following::span[@class='deleteIcon']")
    public WebElement removeClaimantAccount;
    
    @FindBy(xpath="//a[@title='Delete']/following::li[1]")
    public WebElement editLink;
    
    @FindBy(xpath="//button[@title='Save & New']/following::button[@title='Save']")
    public WebElement editSave;
    
    
	public ClaimDetailPage(WebDriver driver) {
		super(driver);
	}
	
	public By pageVerify() {
		By page = By.xpath("//p[text()='Claim']/following::span[1]");
		return page;
	}
	
	public void verifyClaim(String methodName, String tcName, ReportGenerator generator) throws Exception{
		
		Thread.sleep(4000);
		generator.childReport("Verify the Claim Owner");
		String own = GoogleSheetAPI.ReadData(methodName, tcName, "owner");
		String test = owner.getAttribute("textContent");
		assertEquals(own, test);
		SeleniumUtils.highLightElement(owner, driver);
		
		Thread.sleep(2000);
		generator.childReport("Verify the Claim Status");
		String status = GoogleSheetAPI.ReadData(methodName, tcName, "claimstatus");
		String test1 = claimStatus.getAttribute("textContent");
		assertEquals(test1,status);
		SeleniumUtils.highLightElement(claimStatus, driver);
		
		Thread.sleep(1000);
		SeleniumUtils.highLightElement(firstName, driver);
		generator.childReport("Verify the Claim first name");
		assertEquals(firstName.getAttribute("textContent"),GoogleSheetAPI.ReadData(methodName, tcName, "firstname"));
		
		Thread.sleep(3000);
		SeleniumUtils.highLightElement(claimantAccount, driver);
		generator.childReport("Verify the Claimant account");
		String cAccount = claimantAccount.getText();
		if(Form102Page.form102LastName==null) {
			String expAccount = GoogleSheetAPI.ReadData(methodName, tcName, "accountname");
			if(!cAccount.contains(expAccount)) {
				SeleniumUtils.highLightFailedElement(claimantAccount, driver);
				throw new CustomException("The values are not matched");
			}
		}
		else {
			if(!cAccount.contains(Form102Page.form102LastName)) {
				SeleniumUtils.highLightFailedElement(claimantAccount, driver);
				throw new CustomException("The values are not matched");
			}
		}
		
		Thread.sleep(1000);
		SeleniumUtils.highLightElement(lastName, driver);
		generator.childReport("Verify the last name");
		String actualLN = lastName.getText();
		if(Form102Page.form102LastName==null) {
			String expLN = GoogleSheetAPI.ReadData(methodName, tcName, "lastname");
			assertEquals(actualLN, expLN);
		}
		else
			assertEquals(lastName.getText(), Form102Page.form102LastName);
		
		Thread.sleep(2000);
		SeleniumUtils.highLightElement(claimSSN, driver);
		generator.childReport("Verify the SSN");
		String ssns = GoogleSheetAPI.ReadData(methodName, tcName, "ssn");
		String assn = claimSSN.getText();
		assertEquals(assn,ssns);	
	}
	
	public void deleteClaim(String methodName, String tcName, ReportGenerator reporter) throws InterruptedException {
		
		Thread.sleep(3000);
		reporter.childReport("Verify the Delete link");
		SeleniumUtils.highLightElement(deleteLink, driver);
		deleteLink.click();
		
		Thread.sleep(3000);
		reporter.childReport("Verify the Delete button");
		SeleniumUtils.highLightElement(deleteButton, driver);
		deleteButton.click();
	}
	
	public ClaimTaskObjectPage openTask(String methodName, String tcName, ReportGenerator generator) throws Exception{
        generator.childReport("Related list> Task tab clicked");
        Thread.sleep(2000);
        tasks.click();
        generator.childReport("Latest task clicked");
        Thread.sleep(3000);
        tasksClick.click();
        return new ClaimTaskObjectPage(driver);
    }

	public AccountPage clickOnClaimantAccount(String methodName, ReportGenerator reporter) throws InterruptedException {
		Thread.sleep(5000);
		SeleniumUtils.highLightElement(claimantAccountPage, driver);
		claimantAccountPage.click();
		return  new AccountPage(driver);
	}

	public AccountPage clickOnEmployerAccount(String methodName, ReportGenerator reporter) throws InterruptedException {

		Thread.sleep(5000);
		SeleniumUtils.highLightElement(employerAccount, driver);
		employerAccount.click();
		return  new AccountPage(driver);
	}
	
	public void policyNumberVerify(String methodName, String tcName, ReportGenerator generator) throws Exception {

		Thread.sleep(5000);

		SeleniumUtils.presenceOfElement(driver, pageVerify());
		generator.childReport("Claim Detail Page");
		SeleniumUtils.highLightElement(claimNumberVerify, driver);

		Thread.sleep(5000);
		generator.childReport("Carrier Account Verified");
		String carrierExp = carriernameExp.getText();
		TestUtils.compareText(carrierExp, GoogleSheetAPI.ReadData(methodName, tcName, "carrier account verify"),
				carriernameExp, driver);

		Thread.sleep(5000);
		generator.childReport("Policy No Verified");
		String policy = policyNoVerify.getAttribute("textContent");
		String policy1 = policyNoVerify.getText();
		TestUtils.compareText(policy1, GoogleSheetAPI.ReadData(methodName, tcName, "policy no"), policyNoVerify, driver);

	}

	public ClaimTaskObjectPage nonpolicyNumberVerify(String methodName, String tcName, ReportGenerator generator)
			throws Exception {

		Thread.sleep(5000);

		SeleniumUtils.presenceOfElement(driver, pageVerify());
		generator.childReport("Claim Detail Page");
		SeleniumUtils.highLightElement(claimNumberVerify, driver);

		Thread.sleep(5000);
		String carrierExp = carriernameExp.getText();
		if (carrierExp==null) {

			SeleniumUtils.highLightElement(carriernameExp, driver);
			generator.childReport("Carrier Account Null");
		}

		Thread.sleep(5000);
		generator.childReport("Policy No Verified");
		String policy = policyNoVerify.getAttribute("textContent");
		TestUtils.compareText(policy, GoogleSheetAPI.ReadData(methodName, tcName, "policy no"), policyNoVerify, driver);
		return new ClaimTaskObjectPage(driver);

	}
	
	public void editClaimantSection(String methodName, String tcName, ReportGenerator generator) throws Exception{
        Thread.sleep(4000);
        generator.childReport("Edit button clicked");
        SeleniumUtils.highLightElement(editLink, driver);
        editLink.click();
        
        generator.childReport("Claimant First Name entered");
        Thread.sleep(4000);
        SeleniumUtils.highLightElement(editFirstName, driver);
        editFirstName.clear();
        editFirstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant fn"));
        
        generator.childReport("Claimant Last Name entered");
        SeleniumUtils.highLightElement(editLastName, driver);
        editLastName.clear();
        editLastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimant ln"));
        
        generator.childReport("Claimant SSN entered");
        SeleniumUtils.highLightElement(editSSN, driver);
        editSSN.clear();
        editSSN.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "ssn"));
        
        generator.childReport("Claimant DOB entered");
        SeleniumUtils.highLightElement(editDOB, driver);
        editDOB.clear();
        editDOB.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "dob"));
        
        generator.childReport("Claimant Account removed");
        SeleniumUtils.highLightElement(removeClaimantAccount, driver);
        removeClaimantAccount.click();
        
        Thread.sleep(1000);
        generator.childReport("Save button clicked");
        SeleniumUtils.highLightElement(editSave, driver);
        editSave.click();
        Thread.sleep(2000);
    }

}
