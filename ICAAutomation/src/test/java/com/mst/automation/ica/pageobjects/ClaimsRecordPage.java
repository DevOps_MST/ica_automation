package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.CommonUtils;
import com.mst.automation.ica.utils.SeleniumUtils;

public class ClaimsRecordPage extends DriverClass {

	/**
	 * @author Infant Raja Marshall
	 * Created date: 10/26/2017 
	 * Last Edited by: Infant Raja Marshall...
	 * Last Edited date: 10/26/2017 
	 * Description: To check permissions for Claims Record Page.
	 *
	 */
	
	@FindBy(how = How.XPATH, using = "//h2/a[text()='Claims']")
	public WebElement claimsLabel;
	
	public By label() {
		By searchClaims = By.xpath("//h2/a[text()='Claims']");
		return searchClaims;
	}
	
	@FindBy(how = How.XPATH, using = "//h2/a[text()='Claim Tasks']")
	public WebElement claimsTaskLabel;
	
	public By tasklabel() {
		By searchClaimTask = By.xpath("//h2/a[text()='Claim Tasks']");
		return searchClaimTask;
	}
	

	
	

	public ClaimsRecordPage(WebDriver driver) {
		super(driver);
		
	}
	
	/*Actions that to check the permission sets for that appropriate record*/
	
	public ClaimsEditPage recordPageClaims(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, label());
		SeleniumUtils.highLightElement(claimsLabel, driver);
		String str =".slds-cell-edit.cellContainer a";
		CommonUtils.tableIteration(str, GoogleSheetAPI.ReadData(methodName, tcName, "record name"), driver);
		return new ClaimsEditPage(driver);
	}
	public ClaimTaskObjectPage recordPageClaimTask(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, tasklabel());
		SeleniumUtils.highLightElement(claimsTaskLabel, driver);
		String str =".slds-cell-edit.cellContainer a";
		CommonUtils.tableIteration(str, ClaimTaskObjectPage.taskno, driver);
		return new ClaimTaskObjectPage(driver);
	}
}
