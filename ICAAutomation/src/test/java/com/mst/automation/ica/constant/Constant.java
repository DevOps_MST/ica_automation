package com.mst.automation.ica.constant;
/**
 * 
 * @author Ashok Kumar Ganesan
 * Created date: Jan 5, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: It contains the constant values which 
 * is used through out the project.
 */
public class Constant {
	
	private Constant() {
		
	}
	//chrome driver location in the server
	public static final String chromeDriver = System.getenv("CHROME_DRIVER");

	// Binary location of chrome in the server
	public static final String chromeBinary = System.getenv("CHROME_BINARY");
	
    public static final String CHROMEDRIVER = /*System.getenv("CHROME_DRIVER");*/"src/test/resources/drivers/chromedriver.exe";
    public static final String FIREFOXDRIVER = "src/test/resources/drivers/geckodriver.exe";
    public static final String PROPERTYFILEPATH ="src/test/resources/test.properties";
    public static final String REPORTPATH= "src/test/resources/report/report.html";
    public static final String SCREENSHOTPATH = "src/test/resources/report/screenshot/";
    public static final String datafilePath = "src/test/resources/Excels/Testdata.xlsx";
    public static final String UPLOADFILE = "document/ica.exe";
}
