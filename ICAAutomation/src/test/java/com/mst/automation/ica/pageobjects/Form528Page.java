/**
 * 
 */
package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 18-Jan-2018
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form528Page extends DriverClass{
	
	@FindBy(xpath = "//div[2][contains(.,'INDUSTRIAL COMMISSION OF ARIZONA')]")
	public WebElement ica528Verify;
	
	public By element() {
		By open = By.xpath("//div[2][contains(.,'INDUSTRIAL COMMISSION OF ARIZONA')]");
		return open;
	}
	
	@FindBy(xpath = "//label[text()='Injured Worker Last Name']/following::input[1]")
	public WebElement injuredworkerfirstname;
	
	@FindBy(xpath = "//label[text()='Claimant First Name']/following::input[1]")
	public WebElement claimantfirstname;	
	
	@FindBy(xpath = "//label[text()='Social Security No.:']/following::input[1]")
	public WebElement socialsecuritynumber;
	
	@FindBy(xpath = "//label[text()='Injured Worker Last Name']/following::input[1]")
	public WebElement firstname;
	
	@FindBy(xpath = "//label[text()='Date of Injury']/following::a[1]")
	public WebElement dateofinjury;
	
	@FindBy(xpath = "//label[text()='ICA Claim No.:']/following::input[1]")
	public WebElement icaclaimno;
	
	@FindBy(xpath = "//label[text()='Defendent Employer']/following::input[1]")
	public WebElement defendentemployer;
	
	@FindBy(xpath = "//label[contains(.,'Reopening is requested based on the new, ')]/following::textarea[1] ")
	public WebElement requestreopening;
	
	@FindBy(xpath = "//label[text()=' 1. Check one of the following:']/following::input[1]")
	public WebElement checkone;
	
	public By iframe() {
		By user=By.cssSelector("iframe[class='cke_wysiwyg_frame cke_reset']");
		return user;
	}
	
	@FindBy(css = "iframe[class='cke_wysiwyg_frame cke_reset']")
	public WebElement frame;
	
	@FindBy(xpath = "html/body")
	public WebElement workerDescribe;
	
	public By dateverify() {
		By high=By.xpath("//th[1][contains(.,'reopening is REQUIRED.')]/following::a[16]");
		return high;
	}
	
	@FindBy(xpath = "//th[1][contains(.,'reopening is REQUIRED.')]/following::a[16]")
	public WebElement date;
	
	@FindBy(xpath = "//th[1][contains(.,'reopening is REQUIRED.')]/following::textarea[2]")
	public WebElement address;
	
	@FindBy(xpath = "//th[1][contains(.,'reopening is REQUIRED.')]/following::input[2]")
	public WebElement telephoneno;
	
	@FindBy(xpath = "//th[1][contains(.,'reopening is REQUIRED.')]/following::input[3]")
	public WebElement city;
	
	@FindBy(xpath = "//th[1][contains(.,'reopening is REQUIRED.')]/following::input[4]")
	public WebElement state;
	
	@FindBy(xpath = "//th[1][contains(.,'reopening is REQUIRED.')]/following::input[5]")
	public WebElement zipcode;
	
	@FindBy(xpath = "//th[1][contains(.,'reopening is REQUIRED.')]/following::input[6]")
	public WebElement submitteremail;
	
	@FindBy(css = "input[type='Submit']")
	public WebElement submit;
	
	public By verify() {
		By result = By.xpath("//td[contains(.,'Your document')]");
		return result;
	}
	
	@FindBy(xpath = "//td[contains(.,'Your document')]")
	public WebElement verifysuccess;
	
	public By community() {
		By user = By.cssSelector("iframe[title='Visualforce Page component container']");
		return user;
	}

	@FindBy(css = "iframe[title='Visualforce Page component container']")
	public WebElement frameswitch;
	
	
	public Form528Page(WebDriver driver){
		 super(driver);
	 }

	public void fillingForm528(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica528Verify, driver);
		generator.childReport("ICA form name verified");
		
		
		SeleniumUtils.highLightElement(injuredworkerfirstname, driver);
		injuredworkerfirstname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "injuredworkerfirstname"));
		generator.childReport("injuredworkername entered");
		
		SeleniumUtils.highLightElement(claimantfirstname, driver);
		claimantfirstname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantfirstname"));
		generator.childReport("claimantfirstname entered");
		
		SeleniumUtils.highLightElement(socialsecuritynumber, driver);
		socialsecuritynumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "socialsecuritynumber"));
		generator.childReport("socialsecuritynumber entered");
		
		SeleniumUtils.highLightElement(firstname, driver);
		firstname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "firstname"));
		generator.childReport("firstname entered");
		
		SeleniumUtils.highLightElement(dateofinjury, driver);
		dateofinjury.click();
		generator.childReport("dateofinjury entered");
		
		SeleniumUtils.highLightElement(icaclaimno, driver);
		icaclaimno.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "icaclaimno"));
		generator.childReport("icaclaimno entered");
		
		SeleniumUtils.highLightElement(defendentemployer, driver);
		defendentemployer.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "defendentemployer"));
		generator.childReport("defendentemployer entered");
		
		SeleniumUtils.highLightElement(requestreopening, driver);
		requestreopening.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "requestreopening"));
		generator.childReport("requestreopening entered");
		
		SeleniumUtils.highLightElement(checkone, driver);
		checkone.click();
		generator.childReport("checkone entered");
		
SeleniumUtils.presenceOfElement(driver, iframe());
		
		SeleniumUtils.switchToFrame(driver, frame);
		
		generator.childReport("Signature frame switched");
		
		SeleniumUtils.highLightElement(workerDescribe, driver);
		workerDescribe.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "workerDescribe"));
		generator.childReport("workerDescribe entered");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.presenceOfElement(driver, dateverify());
		
		SeleniumUtils.highLightElement(date, driver);
		date.click();
		generator.childReport("date entered");
		
		SeleniumUtils.highLightElement(address, driver);
		address.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "address"));
		generator.childReport("address entered");
		
		SeleniumUtils.highLightElement(telephoneno, driver);
		telephoneno.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "telephoneno"));
		generator.childReport("telephoneno entered");
		
		SeleniumUtils.highLightElement(city, driver);
		city.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "city"));
		generator.childReport("city entered");
		
		SeleniumUtils.highLightElement(state, driver);
		state.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "state"));
		generator.childReport("state entered");
		
		SeleniumUtils.highLightElement(zipcode, driver);
		zipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "zipcode"));
		generator.childReport("zipcode entered");
		
		SeleniumUtils.highLightElement(submitteremail, driver);
		submitteremail.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "submitteremail"));
		generator.childReport("submitteremail entered");
		
		SeleniumUtils.highLightElement(submit, driver);
		submit.click();
		generator.childReport("submit entered");
		
		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifysuccess, driver);

}
	public void fillingForm528Community(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);

		generator.childReport("Form frame switched");
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica528Verify, driver);
		generator.childReport("ICA form name verified");
		
		
		SeleniumUtils.highLightElement(injuredworkerfirstname, driver);
		injuredworkerfirstname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "injuredworkerfirstname"));
		generator.childReport("injuredworkerfirstname entered");
		
		SeleniumUtils.highLightElement(claimantfirstname, driver);
		claimantfirstname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantfirstname"));
		generator.childReport("claimantfirstname entered");
				
		SeleniumUtils.highLightElement(socialsecuritynumber, driver);
		socialsecuritynumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "socialsecuritynumber"));
		generator.childReport("socialsecuritynumber entered");
		
		SeleniumUtils.highLightElement(firstname, driver);
		firstname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "firstname"));
		generator.childReport("firstname entered");
		
		SeleniumUtils.highLightElement(dateofinjury, driver);
		dateofinjury.click();
		generator.childReport("dateofinjury entered");
		
		SeleniumUtils.highLightElement(icaclaimno, driver);
		icaclaimno.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "icaclaimno"));
		generator.childReport("icaclaimno entered");
		
		SeleniumUtils.highLightElement(defendentemployer, driver);
		defendentemployer.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "defendentemployer"));
		generator.childReport("defendentemployer entered");
		
		SeleniumUtils.highLightElement(requestreopening, driver);
		requestreopening.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "requestreopening"));
		generator.childReport("requestreopening entered");
		
		SeleniumUtils.highLightElement(checkone, driver);
		checkone.click();
		generator.childReport("checkone entered");
		
SeleniumUtils.presenceOfElement(driver, iframe());
		
		SeleniumUtils.switchToFrame(driver, frame);
		
		generator.childReport("Signature frame switched");
		
		SeleniumUtils.highLightElement(workerDescribe, driver);
		workerDescribe.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "workerDescribe"));
		generator.childReport("workerDescribe entered");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		SeleniumUtils.presenceOfElement(driver, dateverify());
		
		SeleniumUtils.highLightElement(date, driver);
		date.click();
		generator.childReport("date entered");
		
		SeleniumUtils.highLightElement(address, driver);
		address.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "address"));
		generator.childReport("address entered");
		
		SeleniumUtils.highLightElement(telephoneno, driver);
		telephoneno.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "telephoneno"));
		generator.childReport("telephoneno entered");
		
		SeleniumUtils.highLightElement(city, driver);
		city.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "city"));
		generator.childReport("city entered");
		
		SeleniumUtils.highLightElement(state, driver);
		state.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "state"));
		generator.childReport("state entered");
		
		SeleniumUtils.highLightElement(zipcode, driver);
		zipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "zipcode"));
		generator.childReport("zipcode entered");
		
		SeleniumUtils.highLightElement(submitteremail, driver);
		submitteremail.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "submitteremail"));
		generator.childReport("submitteremail entered");
		
		SeleniumUtils.highLightElement(submit, driver);
		submit.click();
		generator.childReport("submit entered");
		
		driver.switchTo().defaultContent();

		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);

		
		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifysuccess, driver);
		generator.childReport("Success message verified");

}
}
