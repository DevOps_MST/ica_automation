/**
 * @author Ashok Kumar Ganesan
 * Created date:
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: 
 */
package com.mst.automation.ica.regressiontestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

/**
 * @author Ashok Kumar Ganesan
 * Created date: Feb 11, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: Submit two 102 form with same last name & SSN.
 * Verify it is flag status 
 */
public class Document102PossibleMatchTaskOwn extends BaseTest{
	
	@Test (groups = {"Form Regression Suite","102 Flow"})
	@Parameters({"env1","userType"})
	
	public void doc102PossibleMatchTaskOwn(String env1,String userType) throws Exception {
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_111";
		
		String Url = TestUtils.getStringFromPropertyFile(env1);
		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");
		
		reporter = new ReportGenerator(getbrowser(), className);
		/*page102 = loginPage.formUrl102();
		page102.fillingForm102(methodName, tcName, reporter);
		page102.browserBack();
		page102.reload(methodName, tcName);*/
		
		SeleniumUtils.switchToNewTab(driver, Url);
		homePage = loginPage.login(user, pwd, reporter);
		docuselect = homePage.documentObjClick(reporter);
		verify102 = docuselect.docu102Verify(methodName, tcName, reporter);
		
		verify102.document102DetailPage(methodName, tcName, reporter);
		homePage = verify102.getClaimNumber(methodName, tcName, reporter);
		claimTaskSelect = homePage.claimTaskObjClick(reporter);
		claimTaskSelect.selectClaimTask(methodName, tcName, reporter);
		
	}	
}
