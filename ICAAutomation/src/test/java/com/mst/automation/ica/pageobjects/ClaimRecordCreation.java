package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 10/26/2017 
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 10/26/2017 
 * Description: To create a new Claim Record
 *
 */

public class ClaimRecordCreation extends DriverClass {

	@FindBy(xpath = "//span[contains(.,'Home')]/following::span[3]")
	public WebElement claimsTab;

	@FindBy(xpath = "//span[text()='New Claim']")
	public WebElement newButton;
	
	public By newButtonPresence() {
		By button = By.xpath("//span[text()='New Claim']");
		return button;
	}

	@FindBy(xpath = "//h2[text()='New Claim']")
	public WebElement popUp_verify;

	public By newClaimpresence() {
		By claimNew = By.xpath("//h2[text()='New Claim']");
		return claimNew;
	}


	@FindBy(xpath = "//h2[text()='New Claim']/following::button[3]")
	public WebElement clickNext;

	@FindBy(xpath = "//span[text()='Claims Info']")
	public WebElement claimsInfo;

	public By claimsInfoPresence() {
		By info = By.xpath("//span[text()='Claims Info']");
		return info;
	}

	@FindBy(xpath = "//span[contains(.,'DOI')]/following::input[1]")
	public WebElement clickDOI;

	@FindBy(xpath = "//span[text()='Claim Type']/following::a[1]")
	public WebElement claimType;

	@FindBy(xpath = "//span[text()='Claim Type']/following::ul/li[5]/a")
	public WebElement timeLoss;

	@FindBy(xpath = "//span[text()='DOI']/following::input[2]")
	public WebElement occupation;

	@FindBy(xpath = "//span[text()='Occupation']/following::li[1]/a[1]")
	public WebElement occupationClick;
	
	public By verifyOccupation() {
		By occ = By.xpath("//span[text()='Occupation']/following::li[1]/a[1]");
		return occ;
	}
	
	@FindBy(xpath = "//span[contains(.,'First Name')]/following::input[1]")
	public WebElement firstName;

	@FindBy(xpath = "//span[contains(.,'Last Name')]/following::input[1]")
	public WebElement lastName;

	@FindBy(xpath = "//span[contains(.,'DOB')]/following::input[1]")
	public WebElement clickDOB;

	@FindBy(xpath = "//span[text()='Sex']/following::a[1]")
	public WebElement sexSelect;

	@FindBy(xpath = "//span[text()='Claims Body Part']/following::a[1]")
	public WebElement bodyPart;

	@FindBy(css = "a[title='HEAD']")
	public WebElement headSelect;

	@FindBy(css = "button[title='Save']")
	public WebElement clickSave;
	
	public By savePresence() {
		By save = By.cssSelector("button[title='Save']");
		return save;
	}
	
	@FindBy(xpath = "//p[@title='Claim Number']/following::span[1]")
	public WebElement verifyClaimNumber;
	
	public By claimNumberVerify() {
		By num = By.xpath("//p[@title='Claim Number']/following::span[1]");
		return num;
	}
	

	public ClaimRecordCreation(WebDriver driver) {
		super(driver);
	}

	/*Create a new record for Claims*/
	
	public ClaimsRecordPage createClaims(String methodName, String tcName, ReportGenerator generator) throws Exception {


		claimsTab.click();
		generator.childReport("Claims tab clicked");
		SeleniumUtils.presenceOfElement(driver, newButtonPresence());
		newButton.click();
		generator.childReport("New button clicked");
		SeleniumUtils.presenceOfElement(driver, newClaimpresence());
		SeleniumUtils.highLightElement(popUp_verify, driver);
		clickNext.click();
		generator.childReport("Next clicked");
		SeleniumUtils.presenceOfElement(driver, claimsInfoPresence());
		SeleniumUtils.highLightElement(claimsInfo, driver);
		clickDOI.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "DOI"));
		generator.childReport("DOI Entered");
		claimType.click();
		timeLoss.click();
		generator.childReport("Claim Type Time Loss");
		occupation.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Occupation"));
		SeleniumUtils.presenceOfElement(driver, verifyOccupation());
		occupationClick.click();
		generator.childReport("Occupation Entered");
		firstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "First Name"));
		generator.childReport("First Name Entered");
		lastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Last Name"));
		generator.childReport("Last Name Entered");
		clickDOB.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "DOB"));
		generator.childReport("DOB Entered");
		bodyPart.click();
		headSelect.click();
		SeleniumUtils.presenceOfElement(driver, savePresence());
		clickSave.click();
		generator.childReport("Save Clicked");
		SeleniumUtils.presenceOfElement(driver, claimNumberVerify());
		SeleniumUtils.highLightElement(verifyClaimNumber, driver);
		return new ClaimsRecordPage(driver);

	}

}
