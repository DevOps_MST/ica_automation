package com.mst.automation.ica.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.mst.automation.ica.constant.Constant;
import com.mst.automation.ica.customexception.CustomException;
import com.mst.automation.ica.extentreport.ExtentReportFactory;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.relevantcodes.extentreports.ExtentReports;

/**
 * 
 * @author Ashok Kumar Ganesan
 * Created date: Jan 5, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: This class contains the methods to load the property file, to compare the text.
 */
public final class TestUtils {
	
	private TestUtils() {
		
	}
	
	private static String userType;
	protected static final File file;
	protected static FileInputStream fileInput;
	protected static final Properties prop =  new Properties();
	
	static{
		file = new File(Constant.PROPERTYFILEPATH);
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			throw new CustomException("File not found" +e);
		}
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			throw new CustomException("IO Exception" +e);
		}
	}

	public static String getStringFromPropertyFile(String key){
		return prop.getProperty(key);
	}

		
	public static Integer getIntegerFromPropertyFile(String key){
		return new Integer(prop.getProperty(key));
	}
	
	public static String getUserType(){
		return userType;
	}

	public static void setUserType(String urlString){
		userType =urlString;
	}

	public static void compareText(String actual,String expected,WebElement element,WebDriver driver) throws InterruptedException {
		if(actual.equals(expected)) {
			SeleniumUtils.highLightElement(element, driver);
		}
		else {
			SeleniumUtils.highLightFailedElement(element, driver);
			throw new CustomException("The actual value "+actual+" is not matched with the expected value "+expected);
		}
	}
	
	public static void descendingOrder(WebDriver driver, ReportGenerator generator) throws InterruptedException {
		Thread.sleep(4000);
		WebElement tab = driver.findElement(By.xpath("//table/thead/tr/th[5]/div/a"));
		By check = By.xpath("//table/thead/tr/th[5]/div/a");
		
		SeleniumUtils.presenceOfElement(driver, check);
		String value = driver.findElement(By.xpath("//table/thead/tr/th[5]/div/a/span[3]")).getText();
		
		if(value.equalsIgnoreCase("Sorted Ascending")) {
			tab.click();
			generator.childReport("Descending Order Clicked");
			Thread.sleep(5000);
		}
		
	}
	
	//To compare the date by using current date+no.of date passed from spreadsheet
    public static void compareDate(String actual,String expected,WebElement element,WebDriver driver) throws InterruptedException {
        SimpleDateFormat gmtDateFormat = new SimpleDateFormat("MM/dd/yyyy");
          Calendar calendar = Calendar.getInstance();
          calendar.add(Calendar.DATE,Integer.parseInt(expected));
          calendar.add(Calendar.DATE,22);
          expected = gmtDateFormat.format(calendar.getTime());
          if(expected.startsWith("0")) {
              expected = expected.substring(1);
              if(expected.charAt(2)=='0') {
                  expected = expected.substring(0,2)+expected.substring(3);
              }
          }
          else {
              if(expected.charAt(3)=='0') {
                  expected = expected.substring(0,3)+expected.substring(4);
              }
          }
          
        if(actual.equals(expected)) {
            SeleniumUtils.highLightElement(element, driver);
        }
        else {
            SeleniumUtils.highLightFailedElement(element, driver);
        }
    }
}

