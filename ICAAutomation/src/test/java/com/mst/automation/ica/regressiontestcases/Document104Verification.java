package com.mst.automation.ica.regressiontestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class Document104Verification extends BaseTest {
	
	@Test (groups = "Regression Suite")
	@Parameters({ "userType", "UrlQA"})
		
		public void form104Document(String userType, String UrlQA) throws Exception {
		
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_033";
		
		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");
		
		String Url2 = TestUtils.getStringFromPropertyFile(UrlQA);
		
		String user2 =TestUtils.getStringFromPropertyFile(userType+".username2");
		String pwd2 = TestUtils.getStringFromPropertyFile(userType+".password2");
		
		reporter = new ReportGenerator(getbrowser(), className);
		homePage = loginPage.loginCommunity(user, pwd, reporter);
		page104 = homePage.formUrl104cu();
		page104.fillingForm104Community(methodName, tcName, reporter);
		SeleniumUtils.switchToNewTab(driver, Url2);
		homePage = loginPage.login(user2, pwd2, reporter);
		docuselect = homePage.documentObjClick(reporter);
		verify104 = docuselect.docu104Verify(methodName, tcName, reporter);
		verify104.verify104Document(methodName, tcName, reporter);

	}

}
