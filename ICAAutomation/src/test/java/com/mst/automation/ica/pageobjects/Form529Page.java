/**
 * 
 */
package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 18-Jan-2018
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form529Page extends DriverClass{
	
	@FindBy(css = "div[class='logoDiv fontLarge']")
	public WebElement ica529Verify;
	
	public By element() {
		By open = By.cssSelector("div[class='logoDiv fontLarge']");
		return open;
	}
	
	@FindBy(xpath = "//label[contains(.,'Last Name')]/following::input[1]")
	public WebElement lastname;
	
	@FindBy(xpath = "//label[contains(.,'Social')]/following::input[1]")
	public WebElement socialsecurity;
	
	@FindBy(xpath = "//label[contains(.,'First')]/following::input[1]")
	public WebElement firstname;
	
	@FindBy(xpath = "//label[contains(.,'Date of Injury')]/following::a[1]")
	public WebElement dateofinjury;
	
	@FindBy(xpath = "//label[contains(.,'ICA Claim')]/following::input[1]")
	public WebElement icaclaimno;
	
	@FindBy(xpath = "//label[contains(.,'Defendant Employer')]/following::textarea[1]")
	public WebElement defendantemployer;
	
	@FindBy(xpath = "//label[contains(.,'Defendant Employer')]/following::input[2]")
	public WebElement claimant;
	
	@FindBy(xpath = "//label[contains(.,'Defendant Employer')]/following::textarea[3]")
	public WebElement requestrearrangement;
	
	public By frame() {
		By user=By.cssSelector("iframe[class='cke_wysiwyg_frame cke_reset']");
		return user;
	}
	
	@FindBy(css = "iframe[class='cke_wysiwyg_frame cke_reset']")
	public WebElement iframe;
	
	@FindBy(xpath = "html/body")
	public WebElement workerDescribe;
	
	@FindBy(xpath = "//th[1][contains(.,'representative is REQUIRED.')]/following::a[16]")
	public WebElement date;
	
	@FindBy(xpath = "//th[1][contains(.,'representative is REQUIRED.')]/following::textarea[2]")
	public WebElement address;
	
	@FindBy(xpath = "//th[1][contains(.,'representative is REQUIRED.')]/following::input[2]")
	public WebElement telephone;
	
	@FindBy(xpath = "//th[1][contains(.,'representative is REQUIRED.')]/following::input[3]")
	public WebElement city;
	
	@FindBy(xpath = "//th[1][contains(.,'representative is REQUIRED.')]/following::input[4]")
	public WebElement state;
	
	@FindBy(xpath = "//th[1][contains(.,'representative is REQUIRED.')]/following::input[5]")
	public WebElement zipcode;
	
	@FindBy(xpath = "//th[1][contains(.,'representative is REQUIRED.')]/following::input[6]")
	public WebElement submitteremail;
	
	@FindBy(css = "input[type='Submit']")
	public WebElement submit;
	
	public By verify() {
		By result = By.xpath("//td[contains(.,'Your document')]");
		return result;
	}
	
	@FindBy(xpath = "//td[contains(.,'Your document')]")
	public WebElement verifysuccess;
	
	public By community() {
		By user = By.cssSelector("iframe[title='Visualforce Page component container']");
		return user;
	}

	@FindBy(css = "iframe[title='Visualforce Page component container']")
	public WebElement frameswitch;
	
	
	public Form529Page(WebDriver driver){
		 super(driver);
	 }
	public void fillingForm529(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica529Verify, driver);
		generator.childReport("ICA form name verified");
		
		SeleniumUtils.highLightElement(lastname, driver);
		lastname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "lastname"));
		generator.childReport("lastname entered");
		
		SeleniumUtils.highLightElement(socialsecurity, driver);
		socialsecurity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "socialsecurity"));
		generator.childReport("socialsecurity entered");
		
		SeleniumUtils.highLightElement(firstname, driver);
		firstname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "firstname"));
		generator.childReport("firstname entered");
		
		SeleniumUtils.highLightElement(dateofinjury, driver);
		dateofinjury.click();
		generator.childReport("dateofinjury entered");
		
		SeleniumUtils.highLightElement(icaclaimno, driver);
		icaclaimno.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "icaclaimno"));
		generator.childReport("icaclaimno entered");
		
		SeleniumUtils.highLightElement(defendantemployer, driver);
		defendantemployer.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "defendantemployer"));
		generator.childReport("defendantemployer entered");
		
		SeleniumUtils.highLightElement(claimant, driver);
		claimant.click();
		generator.childReport("claimant entered");
		
		SeleniumUtils.highLightElement(requestrearrangement, driver);
		requestrearrangement.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "requestrearrangement"));
		generator.childReport("requestrearrangement entered");
		
		SeleniumUtils.presenceOfElement(driver, frame());
		
		SeleniumUtils.switchToFrame(driver, iframe);
		
		generator.childReport("Signature frame switched");
		
		SeleniumUtils.highLightElement(workerDescribe, driver);
		workerDescribe.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "workerDescribe"));
		generator.childReport("workerDescribe entered");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.highLightElement(date, driver);
		date.click();
		generator.childReport("date entered");
		
		SeleniumUtils.highLightElement(address, driver);
		address.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "address"));
		generator.childReport("address entered");
		
		SeleniumUtils.highLightElement(telephone, driver);
		telephone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "telephone"));
		generator.childReport("telephone entered");
		
		SeleniumUtils.highLightElement(city, driver);
		city.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "city"));
		generator.childReport("city entered");
		
		SeleniumUtils.highLightElement(state, driver);
		state.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "state"));
		generator.childReport("state entered");
		
		SeleniumUtils.highLightElement(zipcode, driver);
		zipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "zipcode"));
		generator.childReport("zipcode entered");
		
		SeleniumUtils.highLightElement(submitteremail, driver);
		submitteremail.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "submitteremail"));
		generator.childReport("submitteremail entered");
		
		SeleniumUtils.highLightElement(submit, driver);
		submit.click();
		generator.childReport("submit entered");
		
		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifysuccess, driver);
		


}
	
	public void fillingForm529Community(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);

		generator.childReport("Form frame switched");

		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica529Verify, driver);
		generator.childReport("ICA form name verified");
		
		SeleniumUtils.highLightElement(lastname, driver);
		lastname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "lastname"));
		generator.childReport("lastname entered");
		
		SeleniumUtils.highLightElement(socialsecurity, driver);
		socialsecurity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "socialsecurity"));
		generator.childReport("socialsecurity entered");
		
		SeleniumUtils.highLightElement(firstname, driver);
		firstname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "firstname"));
		generator.childReport("firstname entered");
		
		SeleniumUtils.highLightElement(dateofinjury, driver);
		dateofinjury.click();
		generator.childReport("dateofinjury entered");
		
		SeleniumUtils.highLightElement(icaclaimno, driver);
		icaclaimno.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "icaclaimno"));
		generator.childReport("icaclaimno entered");
		
		SeleniumUtils.highLightElement(defendantemployer, driver);
		defendantemployer.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "defendantemployer"));
		generator.childReport("defendantemployer entered");
		
		SeleniumUtils.highLightElement(claimant, driver);
		claimant.click();
		generator.childReport("claimant entered");
		
		SeleniumUtils.highLightElement(requestrearrangement, driver);
		requestrearrangement.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "requestrearrangement"));
		generator.childReport("requestrearrangement entered");
		
		SeleniumUtils.presenceOfElement(driver, frame());
		
		SeleniumUtils.switchToFrame(driver, iframe);
		
		generator.childReport("Signature frame switched");
		
		SeleniumUtils.highLightElement(workerDescribe, driver);
		workerDescribe.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "workerDescribe"));
		generator.childReport("workerDescribe entered");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		SeleniumUtils.highLightElement(date, driver);
		date.click();
		generator.childReport("date entered");
		
		SeleniumUtils.highLightElement(address, driver);
		address.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "address"));
		generator.childReport("address entered");
		
		SeleniumUtils.highLightElement(telephone, driver);
		telephone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "telephone"));
		generator.childReport("telephone entered");
		
		SeleniumUtils.highLightElement(city, driver);
		city.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "city"));
		generator.childReport("city entered");
		
		SeleniumUtils.highLightElement(state, driver);
		state.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "state"));
		generator.childReport("state entered");
		
		SeleniumUtils.highLightElement(zipcode, driver);
		zipcode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "zipcode"));
		generator.childReport("zipcode entered");
		
		SeleniumUtils.highLightElement(submitteremail, driver);
		submitteremail.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "submitteremail"));
		generator.childReport("submitteremail entered");
		
		SeleniumUtils.highLightElement(submit, driver);
		submit.click();
		generator.childReport("submit entered");
		
		driver.switchTo().defaultContent();

		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);

		
		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifysuccess, driver);
		


}
}
