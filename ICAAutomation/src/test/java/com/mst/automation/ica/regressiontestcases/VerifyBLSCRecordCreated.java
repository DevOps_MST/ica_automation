/**
 * 
 */
package com.mst.automation.ica.regressiontestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.TestUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 27-Nov-2017
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 
 * Description: 
 *
 */
public class VerifyBLSCRecordCreated extends BaseTest {
	@Test (groups = "Regression Suite", priority=1)
	@Parameters({ "userType" })
	
	public void blsRecordCreation (String userType) throws Exception {

		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_017";

		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");

		reporter = new ReportGenerator(getbrowser(), className);
		homePage = loginPage.login(user, pwd, reporter);
		reporter.childReport("Login_Verify");
		claimsrecordpage = homePage.globalValuePassing(methodName, tcName, reporter);
		editPageClaims = claimsrecordpage.recordPageClaims(methodName, tcName, reporter);
		blsRecordPage = editPageClaims.editRecordClick(methodName, tcName, reporter);
		blsRecordPage.recordPageBls(methodName, tcName, reporter);
	}
}
