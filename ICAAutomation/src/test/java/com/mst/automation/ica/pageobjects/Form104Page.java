/**
 * 
 */
package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Aartheeswaran
 * Created date: Jan 18, 2018
 * Last Edited by: Aartheeswaran...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form104Page extends DriverClass {
	
	@FindBy(xpath = "//th/label[contains(.,'ICA Claim No.')]/following::input[1]")
	public WebElement icaClaimNo;
	
	public By community() {
		By user=By.cssSelector("iframe[title='Visualforce Page component container']");
		return user;
	}
	
	@FindBy(css = "iframe[title='Visualforce Page component container']")
	public WebElement frameswitch;
	
	public By element() {
		By ul=By.xpath("//div[1]/div/b[contains(.,'NOTICE OF CLAIM STATUS')]");
		return ul;
	}
	
	@FindBy(xpath = "//div[1]/div/b[contains(.,'NOTICE OF CLAIM STATUS')]")
	public WebElement ica104Verify;
	
	@FindBy(css = "input[type='submit']")
	public WebElement submitbutton;
	
	public By verify() {
		By result = By.xpath("//td[contains(.,'Your document')]");
		return result;
	}
	
	@FindBy(xpath = "//td[contains(.,'Your document')]")
	public WebElement verifysuccess;

	
	
	public Form104Page(WebDriver driver){
		 super(driver);
	 }
	
	
	public void fillingForm104Community(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, community());
		
		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		generator.childReport("Signature frame switched");
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica104Verify, driver);
		generator.childReport("ICA form name verified");
		
		SeleniumUtils.highLightElement(icaClaimNo, driver);
		icaClaimNo.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "ICA Claim No"));
		generator.childReport("ICA Claim No Entered");
		
		SeleniumUtils.highLightElement(submitbutton, driver);
		submitbutton.click();
		generator.childReport("submitbutton entered");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.presenceOfElement(driver, community());
		
		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifysuccess, driver);
		
		
		
	}

}
