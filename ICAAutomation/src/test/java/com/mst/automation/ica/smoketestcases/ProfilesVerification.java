package com.mst.automation.ica.smoketestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.TestUtils;

public class ProfilesVerification extends BaseTest{
	@Test (groups = "Smoke Suite")
	@Parameters({ "userType" })
	
	public void profilesVerification(String userType) throws Exception {

		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_005";
		
		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");

		reporter = new ReportGenerator(getbrowser(), className);
		homePage = loginPage.login(user, pwd, reporter);
		reporter.childReport("Login_Verify");
		profileSetup = homePage.verifyHomePage1();	
		profileNameSelect = profileSetup.searchProfile(methodName, tcName, reporter);
		profileNameSelect.allProfiles(methodName, tcName, reporter);

	}
	
}

