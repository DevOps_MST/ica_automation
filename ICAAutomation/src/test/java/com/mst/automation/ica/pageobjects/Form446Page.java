/**
 * 
 */
package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

/**
 * @author Infant Raja Marshall
 * Created date: 30-Nov-2017
 * Last Edited by: Infant Raja Marshall...
 * Last Edited date: 
 * Description: 
 *
 */
public class Form446Page extends DriverClass {
	
	
	@FindBy(xpath = "html/body/form/div[1]/div[1]/div[2]")
	public WebElement ica446Verify;
	
	@FindBy(xpath = "//h3[contains(.,'Defendent')]/following::input[1]")
	public WebElement defendentLastName;
	
	@FindBy(xpath = "//h3[contains(.,'Defendent')]/following::input[2]")
	public WebElement defendentSociaSecurity;
	
	@FindBy(xpath = "//h3[contains(.,'Defendent')]/following::input[3]")
	public WebElement defendentFirstName;
	
	@FindBy(xpath = "//h3[contains(.,'Defendent')]/following::input[4]")
	public WebElement defendentICAClaimNo;
	
	@FindBy(xpath = "//h3[contains(.,'Defendent')]/following::textarea[1]")
	public WebElement defendentEmployer;
	
	@FindBy(xpath = "//h3[contains(.,'Defendent')]/following::a[1]")
	public WebElement defendentDateOfInjury;
	
	@FindBy(xpath = "//h3[contains(.,'Request')]/following::input[1]")
	public WebElement requestHearing;
	
	@FindBy(xpath = "//h3[contains(.,'Request')]/following::textarea[2]")
	public WebElement requestStateReason;
	
	@FindBy(xpath = "//h3[contains(.,'Request')]/following::input[9]")
	public WebElement requestCity;
	
	@FindBy(xpath = "//h3[contains(.,'Request')]/following::textarea[3]")
	public WebElement requestEstimation;
	
	@FindBy(css = "iframe[class='cke_wysiwyg_frame cke_reset']")
	public WebElement iframe;

	public By iframeVerify() {
		By frame = By.cssSelector("iframe[class='cke_wysiwyg_frame cke_reset']");
		return frame;
	}
	
	@FindBy(xpath = "html/body")
	public WebElement authenticationSignature;
	
	public By toWindow() {
		By switchTo=By.xpath("//h3[contains(.,'Authentication')]/following::a[16]");
		return switchTo;
	}
	
	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::a[16]")
	public WebElement authenticationDate;
	
	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::textarea[2]")
	public WebElement authenticationAddress;
	
	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::input[2]")
	public WebElement authenticationTelephone;
	
	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::input[3]")
	public WebElement authenticationCity;
	
	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::input[4]")
	public WebElement authenticationState;
	
	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::input[5]")
	public WebElement authenticationZipCode;
	
	@FindBy(xpath = "//h3[contains(.,'Authentication')]/following::input[6]")
	public WebElement authenticationEmail;
	
	@FindBy(css = "input[type='submit']")
	public WebElement submitButton;
	
	public By element() {
		By ul=By.xpath("//h3[contains(.,'Defendent')]/following::input[1]");
		return ul;
	}
	
	@FindBy(xpath = "//label[text()='Email']/following::input[1]")
	public WebElement email;
	
	@FindBy(xpath = "//td[contains(.,'Your document')]")
	public WebElement verifySuccess;
	
	@FindBy(xpath = "html/body/form/div[1]/div[2]/div[1]/div/div[1]/div[2]/div[2]/table/tbody/tr[3]/td/span/div/div[1]/fieldset/table/tbody/tr[3]/td/input")
	public WebElement requestCheckBox;
	
	public By verfication() {
		By ver=By.xpath("//td[contains(.,'Your document')]");
		return ver;
	}
	public By community() {
		By user=By.cssSelector("iframe[title='Visualforce Page component container']");
		return user;
	}
	
	@FindBy(css = "iframe[title='Visualforce Page component container']")
	public WebElement frameswitch;
	
	public Form446Page(WebDriver driver){
		 super(driver);
	 }

	/*To fill the 446 Form*/
	public void fillingForm446(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica446Verify, driver);
		generator.childReport("ICA form 446 name verified");
		
		SeleniumUtils.highLightElement(defendentLastName, driver);
		defendentLastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "defendent Last Name"));
		generator.childReport("defendentLastName entered");
		
		SeleniumUtils.highLightElement(defendentSociaSecurity, driver);
		defendentSociaSecurity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "defendentSociaSecurity"));
		generator.childReport("defendentSociaSecurity entered");
	
		SeleniumUtils.highLightElement(defendentFirstName, driver);
		defendentFirstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "defendentFirstName"));
		generator.childReport("defendentFirstName entered");
	
		SeleniumUtils.highLightElement(defendentICAClaimNo, driver);
		defendentICAClaimNo.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "defendentICAClaimNo"));
		generator.childReport("defendentICAClaimNo entered");
		
		SeleniumUtils.highLightElement(defendentEmployer, driver);
		defendentEmployer.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "defendentEmployer"));
		generator.childReport("defendentEmployer entered");
		
		SeleniumUtils.highLightElement(defendentDateOfInjury, driver);
		defendentDateOfInjury.click();
		generator.childReport("defendentDateOfInjury entered");
		
		SeleniumUtils.highLightElement(requestHearing, driver);
		requestHearing.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "requestHearing"));
		generator.childReport("requestHearing entered");
		
		SeleniumUtils.highLightElement(requestStateReason, driver);
		requestStateReason.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "requestStateReason"));
		generator.childReport("requestStateReason entered");
		
		SeleniumUtils.highLightElement(requestCity, driver);
		requestCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "requestCity"));
		generator.childReport("requestCity entered");
		
		SeleniumUtils.highLightElement(requestCheckBox, driver);
		requestCheckBox.click();
		generator.childReport("requestCheckBox entered");
			
		SeleniumUtils.highLightElement(requestEstimation, driver);
		requestEstimation.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "requestEstimation"));
		generator.childReport("requestEstimation entered");
		
		SeleniumUtils.presenceOfElement(driver, iframeVerify());
		
		SeleniumUtils.switchToFrame(driver, iframe);
		
		generator.childReport("Signature frame switched");
		
		SeleniumUtils.highLightElement(authenticationSignature, driver);
		authenticationSignature.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationSignature"));
		generator.childReport("authenticationSignature entered");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.presenceOfElement(driver, toWindow());
		SeleniumUtils.highLightElement(authenticationDate, driver);
		authenticationDate.click();
		generator.childReport("authenticationDate entered");
		
		SeleniumUtils.highLightElement(authenticationAddress, driver);
		authenticationAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationAddress"));
		generator.childReport("authenticationAddress entered");
		
		SeleniumUtils.highLightElement(authenticationTelephone, driver);
		authenticationTelephone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationTelephone"));
		generator.childReport("authenticationTelephone entered");
		
		SeleniumUtils.highLightElement(authenticationCity, driver);
		authenticationCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationCity"));
		generator.childReport("authenticationCity entered");
		
		SeleniumUtils.highLightElement(authenticationState, driver);
		authenticationState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationState"));
		generator.childReport("authenticationState entered");
		
		SeleniumUtils.highLightElement(authenticationZipCode, driver);
		authenticationZipCode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationZipCode"));
		generator.childReport("authenticationZipCode entered");
		
		SeleniumUtils.highLightElement(email, driver);
		email.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "email"));
		generator.childReport("email entered");
		
		SeleniumUtils.highLightElement(submitButton, driver);
		submitButton.click();
		generator.childReport("submitButton clicked");
		
		SeleniumUtils.presenceOfElement(driver, verfication());
		SeleniumUtils.highLightElement(verifySuccess, driver);


}
public void fillingForm446Community(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
	SeleniumUtils.presenceOfElement(driver, community());
	
	SeleniumUtils.switchToFrame(driver, frameswitch);
	
	generator.childReport("Signature frame switched");
	
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(ica446Verify, driver);
		generator.childReport("ICA form 446 name verified");
		
		SeleniumUtils.highLightElement(defendentLastName, driver);
		defendentLastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "defendent Last Name"));
		generator.childReport("defendentLastName entered");
		
		SeleniumUtils.highLightElement(defendentSociaSecurity, driver);
		defendentSociaSecurity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "defendentSociaSecurity"));
		generator.childReport("defendentSociaSecurity entered");
	
		SeleniumUtils.highLightElement(defendentFirstName, driver);
		defendentFirstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "defendentFirstName"));
		generator.childReport("defendentFirstName entered");
	
		SeleniumUtils.highLightElement(defendentICAClaimNo, driver);
		defendentICAClaimNo.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "defendentICAClaimNo"));
		generator.childReport("defendentICAClaimNo entered");
		
		SeleniumUtils.highLightElement(defendentEmployer, driver);
		defendentEmployer.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "defendentEmployer"));
		generator.childReport("defendentEmployer entered");
		
		SeleniumUtils.highLightElement(defendentDateOfInjury, driver);
		defendentDateOfInjury.click();
		generator.childReport("defendentDateOfInjury entered");
		
		SeleniumUtils.highLightElement(requestHearing, driver);
		requestHearing.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "requestHearing"));
		generator.childReport("requestHearing entered");
		
		SeleniumUtils.highLightElement(requestStateReason, driver);
		requestStateReason.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "requestStateReason"));
		generator.childReport("requestStateReason entered");
		
		SeleniumUtils.highLightElement(requestCity, driver);
		requestCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "requestCity"));
		generator.childReport("requestCity entered");
		
		SeleniumUtils.highLightElement(requestCheckBox, driver);
		requestCheckBox.click();
		generator.childReport("requestCheckBox entered");
			
		SeleniumUtils.highLightElement(requestEstimation, driver);
		requestEstimation.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "requestEstimation"));
		generator.childReport("requestEstimation entered");
		
		SeleniumUtils.presenceOfElement(driver, iframeVerify());
		
		SeleniumUtils.switchToFrame(driver, iframe);
		
		generator.childReport("Signature frame switched");
		
		SeleniumUtils.highLightElement(authenticationSignature, driver);
		authenticationSignature.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationSignature"));
		generator.childReport("authenticationSignature entered");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		SeleniumUtils.presenceOfElement(driver, toWindow());
		SeleniumUtils.highLightElement(authenticationDate, driver);
		authenticationDate.click();
		generator.childReport("authenticationDate entered");
		
		SeleniumUtils.highLightElement(authenticationAddress, driver);
		authenticationAddress.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationAddress"));
		generator.childReport("authenticationAddress entered");
		
		SeleniumUtils.highLightElement(authenticationTelephone, driver);
		authenticationTelephone.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationTelephone"));
		generator.childReport("authenticationTelephone entered");
		
		SeleniumUtils.highLightElement(authenticationCity, driver);
		authenticationCity.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationCity"));
		generator.childReport("authenticationCity entered");
		
		SeleniumUtils.highLightElement(authenticationState, driver);
		authenticationState.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationState"));
		generator.childReport("authenticationState entered");
		
		SeleniumUtils.highLightElement(authenticationZipCode, driver);
		authenticationZipCode.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "authenticationZipCode"));
		generator.childReport("authenticationZipCode entered");
		
		SeleniumUtils.highLightElement(email, driver);
		email.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "email"));
		generator.childReport("email entered");
		
		SeleniumUtils.highLightElement(submitButton, driver);
		submitButton.click();
		generator.childReport("submitButton clicked");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		SeleniumUtils.presenceOfElement(driver, verfication());
		SeleniumUtils.highLightElement(verifySuccess, driver);


}
}
