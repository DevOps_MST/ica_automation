package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

public class NewClaimRequestPage extends DriverClass{
	
	@FindBy(xpath = "//h2[text()='Request For New Claim']")
	public WebElement verifyReuqestPage;
	
	public By element() {
		By ul=By.xpath("//h2[text()='Request For New Claim']");
		return ul;
	}
	
	@FindBy(xpath = "//form/input[1]")
	public WebElement firstName;
	
	@FindBy(xpath = "//form/input[2]")
	public WebElement lastName;
	
	@FindBy(xpath = "//form/input[3]")
	public WebElement claimNumber;
	
	@FindBy(xpath = "//form/div/input")
	public WebElement doi;
	
	@FindBy(xpath = "//form/input[4]")
	public WebElement location;
	
	@FindBy(xpath = "//form/button/span[text()='Submit']")
	public WebElement submitClick;
	
	public By welcome() {
		By check = By.xpath("//span[contains(.,'Welcome!')]");
		return check;
	}
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'Welcome!')]")
	public WebElement welcome_high;

	

	public NewClaimRequestPage(WebDriver driver) {
		super(driver);
	}
	
	public ClaimTaskSelect fillingNewClaimRequestApprove(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(verifyReuqestPage, driver);
		
		firstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "First name"));
		lastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Last name"));
		claimNumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claim number"));
		doi.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "DOI"));
		location.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "Location"));
		submitClick.click();
		Thread.sleep(5000);
		SeleniumUtils.alertAccept(driver);
		SeleniumUtils.elementToBeClickable(driver, welcome());
		SeleniumUtils.highLightElement(welcome_high, driver);
		return new ClaimTaskSelect(driver);
		
	}
}
