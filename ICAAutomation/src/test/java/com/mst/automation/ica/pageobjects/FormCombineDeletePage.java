package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;

public class FormCombineDeletePage extends DriverClass{
	
	public By element() {
		By ul=By.xpath("//label[text()='ICA Claim Number']/following::input[1]");
		return ul;
	}
	
	@FindBy(xpath = "//label[text()='ICA Claim Number']/following::input[1]")
	public WebElement icaClaimNumber;
	
	@FindBy(xpath = "//table//../td/label/following::select[1]")
	public WebElement picklist;
	
	
	public By combineSection() {
		By combine=By.xpath("//h3[contains(.,' combine the above file ')]/following::a[1]");
		return combine;
	}
	
	@FindBy(xpath = "//h3[contains(.,' combine the above file ')]/following::a[1]")
	public WebElement icaNotificationDate;
	
	@FindBy(xpath = "//h3[contains(.,' combine the above file ')]/following::input[2]")
	public WebElement combineClaimNumber;
	
	@FindBy(xpath = "//h3[contains(.,' combine the above file ')]/following::input[3]")
	public WebElement claimantFirstName;
	
	@FindBy(xpath = "//h3[contains(.,' combine the above file ')]/following::input[4]")
	public WebElement claimantLastName;
	
	@FindBy(xpath = "//h3[contains(.,' combine the above file ')]/following::a[2]")
	public WebElement doi;
	
	@FindBy(xpath = "//h3[contains(.,'CHANGE(S) AND/OR CORRECTION(S)')]/following::input[1]")
	public WebElement changeClaimantFirstname;
	
	@FindBy(xpath = "//h3[contains(.,'CHANGE(S) AND/OR CORRECTION(S)')]/following::input[2]")
	public WebElement changeClaimantLastname;
	
	@FindBy(xpath = "//h3[contains(.,'CHANGE(S) AND/OR CORRECTION(S)')]/following::a[1]")
	public WebElement doiclaimant;
	
	public By deleteSection() {
		By delete=By.xpath("//h3[contains(.,' DELETION OF NOTIFICATION ')]");
		return delete;
	}
	
	@FindBy(xpath = "//h3[contains(.,' DELETION OF NOTIFICATION ')]")
	public WebElement deleteSectionVerify;
	
	@FindBy(xpath = "//h3[contains(.,' DELETION OF NOTIFICATION ')]/following::input[9]")
	public WebElement otherCheckbox;
	
	@FindBy(xpath = "//h3[contains(.,' DELETION OF NOTIFICATION ')]/following::textarea[1]")
	public WebElement otherReason;
	

	@FindBy(css = "input[type='submit']")
	public WebElement submitbutton;
	
	public By community() {
		By user=By.cssSelector("iframe[title='Visualforce Page component container']");
		return user;
	}
	
	@FindBy(css = "iframe[title='Visualforce Page component container']")
	public WebElement frameswitch;
	
	public By verify() {
		By result = By.xpath("//td[contains(.,'Your document')]");
		return result;
	}
	
	@FindBy(xpath = "//td[contains(.,'Your document')]")
	public WebElement verifySuccess;

	

	public FormCombineDeletePage(WebDriver driver) {
		super(driver);
	}
	
	public void fillingFormCombineCommunity(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
		SeleniumUtils.presenceOfElement(driver, community());
		
		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		generator.childReport("Signature frame switched");
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(icaClaimNumber, driver);
		icaClaimNumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "icaClaimNumber"));
		generator.childReport("ICA Combine form name verified");
		
		SeleniumUtils.dropDown(picklist, GoogleSheetAPI.ReadData(methodName, tcName, "Combine"));
		generator.childReport("Combine Selected");
		
		SeleniumUtils.presenceOfElement(driver, combineSection());
		icaNotificationDate.click();
		generator.childReport("ICA Notification date entered");
		combineClaimNumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "icaClaimNumber"));
		generator.childReport("Claimant Claim Number entered");
		claimantFirstName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantFirstName"));
		generator.childReport("Claimant First name entered");
		claimantLastName.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantLastName"));
		generator.childReport("Claimant Last name entered");
		doi.click();
		generator.childReport("Claimant Date of Injury entered");
		
		changeClaimantFirstname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantFirstName"));
		generator.childReport("Claimant Change First name entered");
		changeClaimantLastname.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "claimantLastName"));
		generator.childReport("Claimant Change Last name entered");
		doiclaimant.click();
		generator.childReport("Claimant Date of Injury entered");
		
		SeleniumUtils.highLightElement(submitbutton, driver);
		submitbutton.click();
		generator.childReport("submitbutton entered");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifySuccess, driver);
		
		
	}
	
	public void fillingFormDeleteCommunity(String methodName, String tcName, ReportGenerator generator) throws Exception {
		
	SeleniumUtils.presenceOfElement(driver, community());
		
		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		generator.childReport("Signature frame switched");
		
		SeleniumUtils.presenceOfElement(driver, element());
		SeleniumUtils.highLightElement(icaClaimNumber, driver);
		icaClaimNumber.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "icaClaimNumber"));
		generator.childReport("ICA Combine form name verified");
		
		SeleniumUtils.dropDown(picklist, GoogleSheetAPI.ReadData(methodName, tcName, "Delete"));
		generator.childReport("Delete Selected");
		
		SeleniumUtils.presenceOfElement(driver, deleteSection());
		SeleniumUtils.highLightElement(deleteSectionVerify, driver);
		generator.childReport("Delete Section verified");
		
		otherCheckbox.click();
		generator.childReport("Other Checkbox Clicked");
		otherReason.sendKeys(GoogleSheetAPI.ReadData(methodName, tcName, "otherReason"));
		
		SeleniumUtils.highLightElement(submitbutton, driver);
		submitbutton.click();
		generator.childReport("submitbutton entered");
		
		driver.switchTo().defaultContent();
		
		SeleniumUtils.presenceOfElement(driver, community());

		SeleniumUtils.switchToFrame(driver, frameswitch);
		
		SeleniumUtils.presenceOfElement(driver, verify());
		SeleniumUtils.highLightElement(verifySuccess, driver);
		
	}

}
