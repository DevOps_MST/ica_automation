package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class Document529Page extends DriverClass {
	
	public By listView() {
		By list = By.cssSelector("input[data-aura-class=\"uiInput uiInputTextForAutocomplete uiInput--default uiInput--input\"]");
		return list;
	}

	public Document529Page(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath = "//h3/button/span[text()='Claim Description']/following::span[3]")
	public WebElement claimantLastName;
	
	@FindBy(xpath = "//div[1]/span[text()='Social Security No.']/following::span[5]")
	public WebElement socialSecurityNo;
	
	@FindBy(xpath = "//div[1]/span[text()='First Name']/following::span[2]")
	public WebElement clmntFirstName;
	
	@FindBy(xpath = "//div[1]/span[text()='ICA Claim No.']/following::span[2]")
	public WebElement icaClaimNo;
	
	@FindBy(xpath = "//div[1]/span[text()='Defendant Employer']/following::span[2]")
	public WebElement defendantEmployee;
	
	@FindBy(xpath = "//div[1]/span[text()='Reason for Request']/following::span[2]")
	public WebElement reasonForRequest;
	
	@FindBy(xpath = "//div[1]/span[text()='Signature of person or authorized rep']/following::span[1]")
	public WebElement signatureOfPerson;
	
	@FindBy(xpath = "//div[1]/span[text()='Address']/following::span[2]")
	public WebElement authenticationAddress;
	
	@FindBy(xpath = "//div[1]/span[text()='State']/following::span[2]")
	public WebElement authenticationState;
	
	@FindBy(xpath = "//div[1]/span[text()='City']/following::span[2]")
	public WebElement authenticationCity;
	
	@FindBy(xpath = "//div[1]/span[text()='Zip']/following::span[2]")
	public WebElement authenticationZip;
	
	
	
	
public void verify529Document(String methodName, String tcName, ReportGenerator generator) throws Exception{
		
		SeleniumUtils.highLightElement(claimantLastName, driver);
		String clmntLastName = claimantLastName.getText();
		TestUtils.compareText(clmntLastName, GoogleSheetAPI.ReadData(methodName, tcName, "lastname"), claimantLastName, driver);
		
		SeleniumUtils.highLightElement(socialSecurityNo, driver);
		String socSecNo = socialSecurityNo.getText();
		TestUtils.compareText(socSecNo, GoogleSheetAPI.ReadData(methodName, tcName, "socialsecurity"), socialSecurityNo, driver);
		
		SeleniumUtils.highLightElement(clmntFirstName, driver);
		String clmfstname = clmntFirstName.getText();
		TestUtils.compareText(clmfstname, GoogleSheetAPI.ReadData(methodName, tcName, "firstname"), clmntFirstName, driver);
		
		SeleniumUtils.highLightElement(icaClaimNo, driver);
		String icaclaim = icaClaimNo.getText();
		TestUtils.compareText(icaclaim, GoogleSheetAPI.ReadData(methodName, tcName, "icaclaimno"), icaClaimNo, driver);
		
		SeleniumUtils.highLightElement(defendantEmployee, driver);
		String dfndntEmp = defendantEmployee.getText();
		TestUtils.compareText(dfndntEmp, GoogleSheetAPI.ReadData(methodName, tcName, "defendantemployer"), defendantEmployee, driver);
		
		SeleniumUtils.highLightElement(reasonForRequest, driver);
		String reasForReq = reasonForRequest.getText();
		TestUtils.compareText(reasForReq, GoogleSheetAPI.ReadData(methodName, tcName, "requestrearrangement"), reasonForRequest, driver);
		
		SeleniumUtils.highLightElement(signatureOfPerson, driver);
		String signOfPerson = signatureOfPerson.getText();
		TestUtils.compareText(signOfPerson, GoogleSheetAPI.ReadData(methodName, tcName, "workerDescribe"), signatureOfPerson, driver);
		
		SeleniumUtils.highLightElement(authenticationAddress, driver);
		String authAddr = authenticationAddress.getText();
		TestUtils.compareText(authAddr, GoogleSheetAPI.ReadData(methodName, tcName, "address"), authenticationAddress, driver);
		
		SeleniumUtils.highLightElement(authenticationCity, driver);
		String authCty = authenticationCity.getText();
		TestUtils.compareText(authCty, GoogleSheetAPI.ReadData(methodName, tcName, "city"), authenticationCity, driver);
		
		SeleniumUtils.highLightElement(authenticationState, driver);
		String authStat = authenticationState.getText();
		TestUtils.compareText(authStat, GoogleSheetAPI.ReadData(methodName, tcName, "state"), authenticationState, driver);
		
		SeleniumUtils.highLightElement(authenticationZip, driver);
		String authZip = authenticationZip.getText();
		TestUtils.compareText(authZip, GoogleSheetAPI.ReadData(methodName, tcName, "zipcode"), authenticationZip, driver);
		
		
		
		
}
	

}
