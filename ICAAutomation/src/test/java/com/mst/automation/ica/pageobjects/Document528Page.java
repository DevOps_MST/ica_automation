package com.mst.automation.ica.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.ica.abstractclasses.DriverClass;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.googlesheetapi.GoogleSheetAPI;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class Document528Page extends DriverClass {
	
	public By listView() {
		By list = By.cssSelector("input[data-aura-class=\"uiInput uiInputTextForAutocomplete uiInput--default uiInput--input\"]");
		return list;
	}

	public Document528Page(WebDriver driver) {
		super(driver);
	}
	
	
	@FindBy(xpath = "//div[2]/p[text()='528']/following::div/ul[1]/li[1]/a[1]/span[2][text()='Details']")
	public WebElement detailsTab;	
	
	@FindBy(xpath = "//div[1]/span[text()='Claimant First Name']/following::span[2]")
	public WebElement claimantFirstName;
	
	@FindBy(xpath = "//div[1]/span[text()='Social Security No.:']/following::span[2]")
	public WebElement socialSecNo;
	
	@FindBy(xpath = "//div[1]/span[text()='ICA Claim No.:']/following::span[2]")
	public WebElement icaClaimNo;
	
	@FindBy(xpath = "//div[1]/span[text()='Defendent Employer']/following::span[2]")
	public WebElement defendentEmp;
	
	@FindBy(xpath = "//div[1]/span[text()='Reopening reason']/following::span[5]")
	public WebElement reopenReason;
	
	@FindBy(xpath = "//h3/button/span[text()='Authentication Details']/following::span[2]")
	public WebElement authorizedRep;
	
	@FindBy(xpath = "//h3/button/span[text()='Authentication Details']/following::span[10]")
	public WebElement authorizedAddress;
	
	@FindBy(xpath = "//h3/button/span[text()='Authentication Details']/following::span[18]")
	public WebElement authorizedCity;
	
	@FindBy(xpath = "//h3/button/span[text()='Authentication Details']/following::span[22]")
	public WebElement authorizedState;
	
	@FindBy(xpath = "//h3/button/span[text()='Authentication Details']/following::span[26]")
	public WebElement authorizedZipcode;
	
		
	
public void verify528Document(String methodName, String tcName, ReportGenerator generator) throws Exception{
		
		
		Thread.sleep(2000);
		
		detailsTab.click();	
		
		Thread.sleep(2000);
		
		SeleniumUtils.highLightElement(claimantFirstName, driver);
		String clmntFstName = claimantFirstName.getText();
		TestUtils.compareText(clmntFstName, GoogleSheetAPI.ReadData(methodName, tcName, "claimantfirstname"), claimantFirstName, driver);
		
		SeleniumUtils.highLightElement(socialSecNo, driver);
		String socSecNo = socialSecNo.getText();
		TestUtils.compareText(socSecNo, GoogleSheetAPI.ReadData(methodName, tcName, "socialsecuritynumber"), socialSecNo, driver);
		
		SeleniumUtils.highLightElement(icaClaimNo, driver);
		String icaClaim = icaClaimNo.getText();
		TestUtils.compareText(icaClaim, GoogleSheetAPI.ReadData(methodName, tcName, "icaclaimno"), icaClaimNo, driver);
		
		SeleniumUtils.highLightElement(defendentEmp, driver);
		String defEmp = defendentEmp.getText();
		TestUtils.compareText(defEmp, GoogleSheetAPI.ReadData(methodName, tcName, "defendentemployer"), defendentEmp, driver);
		
		SeleniumUtils.highLightElement(reopenReason, driver);
		String reOpRes = reopenReason.getText();
		TestUtils.compareText(reOpRes, GoogleSheetAPI.ReadData(methodName, tcName, "requestreopening"), reopenReason, driver);
		
		SeleniumUtils.highLightElement(authorizedRep, driver);
		String authRep = authorizedRep.getText();
		TestUtils.compareText(authRep, GoogleSheetAPI.ReadData(methodName, tcName, "workerDescribe"), authorizedRep, driver);
		
		SeleniumUtils.highLightElement(authorizedAddress, driver);
		String authAdd = authorizedAddress.getText();
		TestUtils.compareText(authAdd, GoogleSheetAPI.ReadData(methodName, tcName, "address"), authorizedAddress, driver);
		
		SeleniumUtils.highLightElement(authorizedCity, driver);
		String authCty = authorizedCity.getText();
		TestUtils.compareText(authCty, GoogleSheetAPI.ReadData(methodName, tcName, "city"), authorizedCity, driver);
		
		SeleniumUtils.highLightElement(authorizedState, driver);
		String authStat = authorizedState.getText();
		TestUtils.compareText(authStat, GoogleSheetAPI.ReadData(methodName, tcName, "state"), authorizedState, driver);
		
		SeleniumUtils.highLightElement(authorizedZipcode, driver);
		String authZip = authorizedZipcode.getText();
		TestUtils.compareText(authZip, GoogleSheetAPI.ReadData(methodName, tcName, "zipcode"), authorizedZipcode, driver);
		
		
}

}
