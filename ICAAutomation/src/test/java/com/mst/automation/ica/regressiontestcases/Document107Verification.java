package com.mst.automation.ica.regressiontestcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.ica.basetest.BaseTest;
import com.mst.automation.ica.extentreport.ReportGenerator;
import com.mst.automation.ica.utils.SeleniumUtils;
import com.mst.automation.ica.utils.TestUtils;

public class Document107Verification extends BaseTest {
	
	@Test (groups = "Regression Suite")
	@Parameters({ "userType", "UrlQA"})
		
		public void form107Document(String userType, String UrlQA) throws Exception {
		
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String className = this.getClass().getSimpleName();
		String tcName = "TCICA_034";
		
		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String pwd = TestUtils.getStringFromPropertyFile(userType+".password");
		
		String Url2 = TestUtils.getStringFromPropertyFile(UrlQA);
		
		String user2 =TestUtils.getStringFromPropertyFile(userType+".username2");
		String pwd2 = TestUtils.getStringFromPropertyFile(userType+".password2");
		
		reporter = new ReportGenerator(getbrowser(), className);
		homePage = loginPage.loginCommunity(user, pwd, reporter);
		page107 = homePage.formUrl107cu();
		page107.fillingForm107Community(methodName, tcName, reporter);
		SeleniumUtils.switchToNewTab(driver, Url2);
		homePage = loginPage.login(user2, pwd2, reporter);
		docuselect = homePage.documentObjClick(reporter);
		verify107 = docuselect.docu107Verify(methodName, tcName, reporter);
		verify107.verify107Document(methodName, tcName, reporter);

	}

}
